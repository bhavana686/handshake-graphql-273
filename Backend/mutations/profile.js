
const Company = require("../dbSchema/company");
const Student = require("../dbSchema/student")


const updateCompany = async (args) => {
    let company= await Company.findOneAndUpdate({ companyid: args.companyid }, 
        {
          companyname: args.companyname,
          companylocation: args.companylocation,
            companydescription: args.companydescription,
        },
        {
          new: true,
        })
        if (company) {
        
        
            return { status: 200, message: "COMPANY_UPDATED" };
        }
       else 
       {
        return { status: 500, message: "INTERNAL_SERVER_ERROR" };
     }
};
const updateStudentbasic = async (args) => {
   let student= await Student.findOneAndUpdate({ studentid: args.studentid }, 
        {
          name: args.name,
          dateofbirth: args.dateofbirth,
          city: args.city,
          state: args.state,
          country: args.country,
        },
        {
          new: true,
        })
        console.log(student)
        if (student) {
            return { status: 200, message: "BASIC_UPDATED" };
        }
       else 
       {

        return { status: 500, message: "INTERNAL_SERVER_ERROR" };
     }
};
const updateStudentcareer = async (args) => {
    let student= await Student.findOneAndUpdate({ studentid:args.studentid },
        {
            careerobjective: args.careerobjective,
          },
          {
            new: true,
          })
       
        if (student) {
            return { status: 200, message: "CAREER_UPDATED" };
        }
       else 
       {
        return { status: 500, message: "INTERNAL_SERVER_ERROR" };
     }
};
const updateStudentcontact = async (args) => {
  let student= await Student.findOneAndUpdate({ studentid:args.studentid },
      {
         mail:args.mail,
         phonenumber:args.phonenumber

        },
        {
          new: true,
        })
     
      if (student) {
          return { status: 200, message: "CAREER_UPDATED" };
      }
     else 
     {
      return { status: 500, message: "INTERNAL_SERVER_ERROR" };
   }
};
const updateStudenteducation= async (args) => {
    let student=  await Student.findOneAndUpdate({ studentid: args.studentid, 'education.educationdetailsid': args.educationdetailsid },
    { $set: { 'education.$': args} })
     if (student) {
            return { status: 200, message: "EDUCATION_UPDATED" };
        }
       else 
       {
        return { status: 500, message: "INTERNAL_SERVER_ERROR" };
     }
};

const updateStudentexperience = async (args) => {
    let student=  await Student.findOneAndUpdate({ studentid: args.studentid, 'experience.experiencedetailsid': args.experiencedetailsid },
    { $set: { 'experience.$': args} })
     if (student) {
            return { status: 200, message: "EXPERIENCE_UPDATED" };
        }
       else 
       {
        return { status: 500, message: "INTERNAL_SERVER_ERROR" };
     }
};







exports.updateCompany = updateCompany;
exports.updateStudentbasic = updateStudentbasic;
exports.updateStudentcareer = updateStudentcareer;
exports.updateStudenteducation =updateStudenteducation;
exports.updateStudentexperience = updateStudentexperience;
exports.updateStudentcontact = updateStudentcontact;
