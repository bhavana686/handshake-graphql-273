
const Company = require("../dbSchema/company");
const Student = require("../dbSchema/student")
const jobdetails = require("../dbSchema/jobdetails")


const addJob = async (args) => {
    const newjobdetails = new jobdetails({
        companyid: args.companyid,
        jobtitle: args.jobtitle,
        jobpostingdate: args.jobpostingdate,
        jobapplicationdeadline: args.jobapplicationdeadline,
        joblocation: args.joblocation,
        jobsalary: args.jobsalary,
        jobdescription: args.jobdescription,
        jobcategory: args.jobcategory,
      });
     
      let job= await newjobdetails.save();
     
        if (job) {
            return { status: 200, message: "JOB_ADDED" };
        }
       else 
       {
        return { status: 500, message: "INTERNAL_SERVER_ERROR" };
     }
};

const applyJob = async (args) => {
    const application = {
       
        jobid: args.jobid,
        studentid: args.studentid,
        name: args.name,
        mail: args.mail,
        applicationstatus:args.applicationstatus

      };
      let app= await jobdetails.findOneAndUpdate({ jobid: args.jobid }, { $push: { applications: application } })
        if (app) {
            return { status: 200, message: "JOB_APPLIED" };
        }
       else 
       {
        return { status: 500, message: "INTERNAL_SERVER_ERROR" };
     }
};

exports.addJob = addJob;
exports.applyJob = applyJob;











