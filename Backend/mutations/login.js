
const Company = require("../dbSchema/company");
const Student = require("../dbSchema/student")
const passwordHash = require('password-hash');
const jwt = require('jsonwebtoken');
const { secret } = require('../config');

const studentLogin = async (args) => {
    let student = await Student.find({ mail: args.mail });
    if (student.length === 0) {
        return { status: 401, message: "NO_STUDENT" };
    }
    if (passwordHash.verify(args.password, student[0].password)) {
        const payload = { studentid: student[0].studentid, name: student[0].name, mail: student[0].mail,loggintype:"student" };
        var token = jwt.sign(payload, secret, {
            expiresIn: 1008000
        });
        token = 'JWT ' + token;
        return { status: 200, message: token};
    }
    else {
        return { status: 401, message: "INCORRECT_PASSWORD" };
    }
};

const companyLogin = async (args) => {
    let company = await Company.find({ companymail: args.companymail });
    if (company.length === 0) {
        return { status: 401, message: "NO_COMPANY" };
    }
    if (passwordHash.verify(args.password, company[0].password)) {
        const payload = { companyid: company[0].companyid, companyname: company[0].companyname, companymail: company[0].companymail,loggintype:"company" };
        var token = jwt.sign(payload, secret, {
            expiresIn: 1008000
        });
        token = 'JWT ' + token;
        return { status: 200, message: token};
    }
    else {
        return { status: 401, message: "INCORRECT_PASSWORD" };
    }
};
exports.companyLogin = companyLogin;
exports.studentLogin= studentLogin;
