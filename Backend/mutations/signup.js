const Company = require("../dbSchema/company");
const Student = require("../dbSchema/student")
const passwordHash = require('password-hash');

const studentSignup = async (args) => {
    let hashedPassword = passwordHash.generate(args.password);
    let newUser = new Student({
        name: args.name,
        mail: args.mail,
        password: hashedPassword,
        collegename: args.collegename,
       
    });
    let user = await Student.find({ mail: args.mail});
    if (user.length) {
 
        return { status: 400, message: "STUDENT_EXISTS" };
    }
    let savedUser = await newUser.save();
    if (savedUser) {
        return { status: 200, message: "STUDENT_ADDED" };
    }
    else {
        return { status: 500, message: "INTERNAL_SERVER_ERROR" };
    }
};

const companySignup = async (args) => {
    
    let hashedPassword = passwordHash.generate(args.password);
    let newUser = new Company({
        companyname: args.companyname,
        companymail: args.companymail,
        password: hashedPassword,
        companylocation: args.companylocation,
       
    });
    let user = await Company.find({ companymail: args.companymail });
    if (user.length) {
       
        return { status: 400, message: "COMPANY_EXISTS" };
    }
    let savedUser = await newUser.save();
    if (savedUser) {
        return { status: 200, message: "COMPANY_ADDED" };
    }
    else {
        return { status: 500, message: "INTERNAL_SERVER_ERROR" };
    }
};


exports.companySignup = companySignup;
exports.studentSignup = studentSignup;
