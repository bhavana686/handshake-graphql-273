
const Company = require("../dbSchema/company");
const Student = require("../dbSchema/student")
const jobdetails = require("../dbSchema/jobdetails")


const addEducation = async (args) => {
    const educations = {
        collegename: args.collegename,
        educationlocation:args.educationlocation,
        educationdegree: args.educationdegree,
        educationmajor: args.educationmajor,
        educationyearofpassing:args.educationyearofpassing,
        educationcurrentcgpa: args.educationcurrentcgpa,
        
        };
     let student= await Student.updateOne({ studentid: args.studentid }, { $push: { education: educations }, $sort: { educationyearofpassing: -1 } })
        if (student) {
            return { status: 200, message: "EDUCATION_ADDED" };
        }
       else 
       {
        return { status: 500, message: "INTERNAL_SERVER_ERROR" };
     }
};

const addExperience = async (args) => {
    const experiences = {
        companyname: args.companyname,
        experiencetitle: args.experiencetitle,
        experiencelocation: args.experiencelocation,
        experiencestartdate:args.experiencestartdate,
        experienceenddate: args.experienceenddate,
        experienceworkdescription: args.experienceworkdescription,
            };
     let student=  await Student.updateOne({ studentid: args.studentid }, { $push: { experience: experiences }, $sort: { experienceenddate: -1 } })
     if (student) {
        return { status: 200, message: "EXPERIENCE_ADDED" };
    }
   else 
   {
    return { status: 500, message: "INTERNAL_SERVER_ERROR" };
 }

};



exports.addEducation = addEducation;
exports.addExperience = addExperience;











