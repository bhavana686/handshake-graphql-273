const Company = require("../dbSchema/company");
const Student = require("../dbSchema/student")
const jobdetails = require("../dbSchema/jobdetails")
const passwordHash = require('password-hash');

const studentsappliedtojob = async (args) => {
    let aggr = [
        { $match: { jobid: args.jobid } },

        {
            $lookup: {
                localField: 'applications.studentid',
                from: 'students',
                foreignField: 'studentid',
                as: 'students'
            }
        },
        {
            $project: {
                'applications.applicationid': 1,
                'applications.applicationstatus': 1,
                'applications.name': 1,
                'applications.mail': 1,
                'applications.studentid': 1,

            }

        }
    ];
   let students= await jobdetails.aggregate(aggr);
   console.log("students applied to job")
   console.log(students[0].applications);

    return students[0].applications;
   
};
const alljobs = async (args) => {
    let aggr = [{
        $lookup: {
            localField: 'companyid',
            from: 'companies',
            foreignField: 'companyid',
            as: 'company'
        }
    }, { $unwind: '$company' },
    {
        $project: {


            companyid: 1,
            jobtitle: 1,
            joblocation: 1,
            jobpostingdate: 1,
            jobapplicationdeadline: 1,
            jobdescription: 1,
            jobsalary: 1,
            jobid: 1,
            applications: {
                $filter: {
                    input: "$applications",
                    as: "list",
                    cond: { $in: ["$$list.studentid", [args.studentid]] } //<-- filter sub-array based on condition
                }
            },
            jobcategory: 1,
            "companyname": "$company.companyname",


        }

    }
    ];
    let jobs = await jobdetails.aggregate(aggr);
  
 
    return jobs;
   
};
const studentsAppliedjobs = async (args) => {
  await jobdetails.find({ 'applications.studentid': args.studentid }, (err, student) => {
        if (err) {
        }
        else{
            return student
        }
});
}



exports.studentsappliedtojob =studentsappliedtojob;
exports.alljobs =alljobs;
exports.studentsAppliedjobs =studentsAppliedjobs;

