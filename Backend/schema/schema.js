const graphql = require('graphql');
const Company = require("../dbSchema/company");
const Student = require("../dbSchema/student");
const jobdetails = require("../dbSchema/jobdetails");
const { addJob,applyJob } = require('../mutations/company');
const { studentLogin, companyLogin } = require('../mutations/login');
const { studentSignup, companySignup } = require('../mutations/signup');
const { addEducation, addExperience } = require('../mutations/student');
const { studentsappliedtojob ,alljobs, studentsAppliedjobs} = require('../query/company');
const { updateCompany,updateStudentbasic,updateStudentcontact,updateStudentcareer,updateStudenteducation,updateStudentexperience} = require('../mutations/profile');



const {
    GraphQLObjectType,
    GraphQLString,
    GraphQLSchema,
    GraphQLID,
    GraphQLInt,
    GraphQLList,
    GraphQLNonNull
} = graphql;

const StudentType = new GraphQLObjectType({
    name: 'Student',
    fields: () => ({
        id: { type: GraphQLID },
        studentid:{ type: GraphQLString },
        name: { type: GraphQLString },
        mail: { type: GraphQLString },
        collegename: { type: GraphQLString },
        dateofbirth: { type: GraphQLString },
        city: { type: GraphQLString },
        state: { type: GraphQLString },
        country:{ type: GraphQLString },
        phonenumber: { type: GraphQLString },
        profilepic: { type: GraphQLString },
        careerobjective: { type: GraphQLString },
        skillset: { type: GraphQLString },
        education:{type:GraphQLList(EducationType)},
        experience: {type:GraphQLList(ExperienceType)}
    })
});
const EducationType = new GraphQLObjectType({
    name: 'Education',
    fields: () => ({
        id: { type: GraphQLID },
        educationdetailsid: { type: GraphQLString },
        collegename:{ type: GraphQLString },
        educationlocation: { type: GraphQLString },
        educationdegree: { type: GraphQLString },
        educationmajor: { type: GraphQLString },
        educationyearofpassing: { type: GraphQLString },
        educationcurrentcgpa: { type: GraphQLString },
        
    })
});
const ExperienceType = new GraphQLObjectType({
    name: 'Experience',
    fields: () => ({
        id: { type: GraphQLID },
        experiencedetailsid: { type: GraphQLString },
        companyname: { type: GraphQLString },
        experiencetitle:{ type: GraphQLString },
        experiencelocation: { type: GraphQLString },
        experiencestartdate:{ type: GraphQLString },
        experienceenddate: { type: GraphQLString },
        experienceworkdescription: { type: GraphQLString },
        
    })
});
const CompanyType = new GraphQLObjectType({
    name: 'Company',
    fields: () => ({
        id: { type: GraphQLID },
        companyid:{ type: GraphQLString },
        companyname: { type: GraphQLString },
        companymail: { type: GraphQLString },
        companylocation: { type: GraphQLString },
        companydescription: { type: GraphQLString },
        companyphonenumber: { type: GraphQLString },
        companyprofilepic: { type: GraphQLString },
    })
});
const ApplicationType = new GraphQLObjectType({
    name: 'ApplicationType',
    fields: () => ({
             

        id: { type: GraphQLID },
        applicationid: { type: GraphQLString },
        applicationstatus:{ type: GraphQLString },
        name:{ type: GraphQLString },
        mail: { type: GraphQLString },
        studentid:{ type: GraphQLString },

       
       
    })
    
});

const JobType = new GraphQLObjectType({
    name: 'Job',
    fields: () => ({
        id: { type: GraphQLID },
        jobid: { type: GraphQLString },
        companyid:{ type: GraphQLString },
        jobtitle: {type: GraphQLString},
        jobpostingdate: {type: GraphQLString},
        jobapplicationdeadline:  {type: GraphQLString},
        joblocation:  {type: GraphQLString},
        jobsalary:  {type: GraphQLString},
        jobdescription:  {type: GraphQLString},
        jobcategory:  {type: GraphQLString},
        applications: { type:GraphQLList(ApplicationType) },
        companyname:  {type: GraphQLString},

    })
});
const StatusType = new GraphQLObjectType({
    name: 'Status',
    fields: () => ({
        status: { type: GraphQLString },
        message: { type: GraphQLString}
    })
});




const RootQuery = new GraphQLObjectType({
    name: 'RootQueryType',
    fields: {
        getCompanyprofile: {
            type: CompanyType,
            args: { companyid: { type: GraphQLString } },
            async resolve(parent, args) {
                let user = await Company.find({companyid:args.companyid});
                if (user) {
                    return user[0];
                }
            }
        },
        getStudentprofile: {
            type:   StudentType,
            args: { studentid: { type: GraphQLString } },
            async resolve(parent, args) {
                let user = await Student.find({studentid:args.studentid});
                if (user) {
                    return user[0];
                }
            }
        },
        student: {
            type: StudentType,
            args: { studentid: { type: GraphQLString } },
            async resolve(parent, args) {
                let user = await Student.find({studentid:args.studentid});
                if (user) {
                    return user;
                }
            }
        },
        getAllstudents: {
            type: new GraphQLList(StudentType),
            async resolve(parent) {
                let students= await Student.find({});
                return students;
            }
        },
        getAllotherstudents: {
            type: new GraphQLList(StudentType), 
            args: { studentid: { type: GraphQLString } },

            async resolve(paren,args) {
                let students= await Student.find({ studentid: { $ne: args.studentid }});
                return students;
            }
        },
        getAllcompanyjobs: {
            type: new GraphQLList(JobType),
            args: { companyid: { type: GraphQLString } },
            async resolve(parent, args) {
                let joblist = await jobdetails.find({ companyid: args.companyid});
                
                return joblist;
            }
        },
        getAlljobs: {
            type:new GraphQLList(JobType),
            args: { studentid: { type: GraphQLString } },
            async resolve(parent, args) {
                return alljobs(args);
              
            }
        },
        getStudentsAppliedtojob: {
            type: new GraphQLList(ApplicationType),
            args: { jobid: { type: GraphQLString } },
            async resolve(parent, args) {
                
                return studentsappliedtojob(args);
            }
        },
        getstudentsAppliedjobs:{
            type:new GraphQLList(JobType),
            args: { studentid: { type: GraphQLString } },
            async resolve(parent, args) {
                let joblist = await jobdetails.find({ 'applications.studentid': args.studentid })
              
                return joblist;
              
            }
        },
    //     searchJobs : {),
    //         args : {searchtext : {type : GraphQLString}},
        
    //     async resolve(parent,args){
    //             let job = await jobdetails.find({jobtitle: {$regex: args.searchtext,$options: 'i'} } );
    //             let company= await Company.find({companyname : {$regex : args.searchtext,$options : 'i'}});
    //             if(job)
    //             {
    //                 return job;
    //    
    //         type: new GraphQLList(JobType         }
    //             if(company)
    //             {
    //                 return company
    //             }
    // }}, 
    jobsearch : {
            
        type: new GraphQLList(JobType),
        args : {
               searchtext : {type : GraphQLString}
               },
         async resolve(parent,args){
             var companylist = []
              let comp=  await Company.find({ companyname: { $regex : args.searchtext,$options : 'i'} }, (error, results) => {
                     companylist = results.map(function (company) { return company.companyid; });
                       console.log(companylist)
                     })
                     console.log(comp)
                 let job=await jobdetails.find({$or:[{jobtitle : {$regex : args.searchtext,$options : 'i'} } ,{companyid: {$in: companylist} } ]})

                 if(job)
                     return job;
         

             }
             
         
     
},
    }

});
const Mutation = new GraphQLObjectType({
    name: 'Mutation',
    fields: {
        addCompany: {
            type: StatusType,
            args: {
                companyname: { type: GraphQLString },
                companymail: { type: GraphQLString },
                password: { type: GraphQLString },
                companylocation: { type: GraphQLString }
            },
            async resolve(parent, args) {
                return companySignup(args);
            }
        },
        addStudent: {
            type: StatusType,
            args: {
                name: { type: GraphQLString },
                mail: { type: GraphQLString },
                password: { type: GraphQLString },
               collegename:{ type: GraphQLString }
            },
            async resolve(parent, args) {
                return studentSignup(args);
            }
        },
        studentLogin: {
            type:  StatusType,
            args: {
                mail: { type: GraphQLString },
                password: { type: GraphQLString },
            },
            async resolve(parent, args) {
                return studentLogin(args);
            }
        },
        companyLogin: {
            type:  StatusType,
            args: {
                companymail: { type: GraphQLString },
                password: { type: GraphQLString },
            },
            async resolve(parent, args) {
                return companyLogin(args);
            }
        },
        addJob: {
           
            type:  StatusType,
            args: {
                companyid:{type: GraphQLString},
                jobtitle: {type: GraphQLString},
                jobpostingdate: {type: GraphQLString},
                jobapplicationdeadline:  {type: GraphQLString},
                joblocation:  {type: GraphQLString},
                jobsalary:  {type: GraphQLString},
                jobdescription:  {type: GraphQLString},
                jobcategory:  {type: GraphQLString},
                 
            },
            async resolve(parent, args) {
                return addJob(args);
            }
        },
        addEducation: {
            type:  StatusType,
            args: {
                studentid:{type: GraphQLString},
                collegename:{ type: GraphQLString },
                educationlocation: { type: GraphQLString },
                educationdegree: { type: GraphQLString },
                educationmajor: { type: GraphQLString },
                educationyearofpassing: { type: GraphQLString },
                educationcurrentcgpa: { type: GraphQLString },
                 
            },
            async resolve(parent, args) {
                return addEducation(args);
            }
        },
        addExperience: {
            type:  StatusType,
            args: {
                studentid:{type: GraphQLString},
                companyname: { type: GraphQLString },
                experiencetitle:{ type: GraphQLString },
                experiencelocation: { type: GraphQLString },
                experiencestartdate:{ type: GraphQLString },
                experienceenddate: { type: GraphQLString },
                experienceworkdescription: { type: GraphQLString },
            },
            async resolve(parent, args) {
                return addExperience(args);
            }
        },
        updateCompany: {
            type: StatusType,
            args: {
                companyid:{type: GraphQLString},
                companyname: { type: GraphQLString },
                companylocation: { type: GraphQLString },
                companydescription: { type: GraphQLString },
            },
            resolve(parent, args) {
                return updateCompany(args);
            }
        },
        updateStudentbasic: {
            type: StatusType,
            args: {
                studentid:{type: GraphQLString},
                name: { type: GraphQLString },
                dateofbirth:{ type: GraphQLString },
                city:{ type: GraphQLString },
                state:{ type: GraphQLString },
                country:{ type: GraphQLString },   
            },
            resolve(parent, args) {
              
                return updateStudentbasic(args);
            }
        },
        updateStudentcareer: {
            type: StatusType,
            args: {
                studentid:{type: GraphQLString},
                careerobjective:{ type: GraphQLString },
            },
            resolve(parent, args) {
                return updateStudentcareer(args);
            }
        },
        updateStudentcontact: {
            type: StatusType,
            args: {
                studentid:{type: GraphQLString},
                mail:{ type: GraphQLString },
                phonenumber:{ type: GraphQLString },
            },
            resolve(parent, args) {
                return updateStudentcontact(args);
            }
        },
        updateStudentEducation: {
            type: StatusType,
            args: {
                studentid:{type: GraphQLString},
                educationdetailsid: { type: GraphQLString },
                collegename:{ type: GraphQLString },
                educationlocation: { type: GraphQLString },
                educationdegree: { type: GraphQLString },
                educationmajor: { type: GraphQLString },
                educationyearofpassing: { type: GraphQLString },
                educationcurrentcgpa: { type: GraphQLString },
            },
            resolve(parent, args) {
                return updateStudenteducation(args);
            }
        },
        updateStudentExperience: {
            type: StatusType,
            args: {
                studentid:{type: GraphQLString},
                experiencedetailsid: { type: GraphQLString },
                companyname: { type: GraphQLString },
                experiencetitle:{ type: GraphQLString },
                experiencelocation: { type: GraphQLString },
                experiencestartdate:{ type: GraphQLString },
                experienceenddate: { type: GraphQLString },
                experienceworkdescription: { type: GraphQLString },
            },
            resolve(parent, args) {
                return updateStudentexperience(args);
            }
        },
        applyJob: {
            type: StatusType,
            args: {
                applicationstatus: { type: GraphQLString },
                jobid: { type: GraphQLString },
                studentid: {type: GraphQLString },
                name: {type: GraphQLString },
                mail : {type: GraphQLString },
            },
            resolve(parent, args) {
                return applyJob(args);
            }
        },
    }

});



module.exports = new GraphQLSchema({
    query: RootQuery,
    mutation: Mutation
});