const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
const uuid = require('uuid');


const { Schema } = mongoose;

const company = new Schema({
  companyid: {
    type: String,
    default: uuid.v1,
  },
  companyname: {
    type: String,
  },
  companymail: {
    type: String,
    unique: true,
  },
  password: {
    type: String,

  },
  companylocation: {
    type: String,
  },
  companydescription: {
    type: String,

  },
  companyphonenumber: {
    type: String,

  },
  companyprofilepic: {
    type: String,
  },

});


company.plugin(uniqueValidator);

module.exports = mongoose.model('company', company);
