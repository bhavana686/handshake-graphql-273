const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
const uuid = require('uuid');

const { Schema } = mongoose;

const jobdetails = new Schema({
  jobid: {
    type: String,
    default: uuid.v1,
  },

  companyid: {
    type: String,

  },
  jobtitle: {
    type: String,

  },
  jobpostingdate: {
    type:String ,

  },
  jobapplicationdeadline: {
    type: String,

  },
  joblocation: {
    type: String,

  },
  jobsalary: {
    type: Number,

  },
  jobdescription: {
    type: String,

  },
  jobcategory: {
    type: String,
  },
  applications: [
    {
      applicationid: {
        type: String,
        default: uuid.v1,
      },
      applicationstatus: {
        type: String,

      },
      jobid: {
        type: String,

      },
      studentid: {
        type: String,

      },
      applicationdate: {
        type: String,

      },
      name:{
        type: String
      },
      mail:{
        type:String

      }
      
    },
  ],

});

jobdetails.plugin(uniqueValidator);


module.exports = mongoose.model('jobdetails', jobdetails);
