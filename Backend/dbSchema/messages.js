
const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
const uuid = require('uuid');


const { Schema } = mongoose;

const messages = new Schema({
    

  user1id:String,
  user2id:String,
  user1type:String,
  user2type:String,
  lastmessagetime:String,
  msgid:{ type: String, default: uuid.v1 },
   message: [
    {
      messageid: { type: String, default: uuid.v1 },
      userid:String,
      messagecontent: String,
      messagetime: String,
    },
  ],
  
});


messages.plugin(uniqueValidator);

module.exports = mongoose.model('messages', messages);
