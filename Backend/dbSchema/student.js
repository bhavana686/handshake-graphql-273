
const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
const uuid = require('uuid');


const { Schema } = mongoose;

const student = new Schema({
  studentid: {
    type: String,
    default: uuid.v1,
  },
  name: {
    type: String,
    required: true,
  },
  mail: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  collegename: {
    type: String,
    required: true,
  },
  dateofbirth: {
    type: String,

  },
  city: {
    type: String,

  },
  state: {
    type: String,
  },
  country: {
    type: String,
  },
  phonenumber: {
    type: String,
  },
  profilepic: {
    type: String,
  },
  careerobjective: {
    type: String,
  },
  skillset: {
    type: String,
  },
  applications: { type: Array, required: false },
  registrations: { type: Array, required: false },
  education: [
    {
      educationdetailsid: { type: String, default: uuid.v1 },
      collegename: String,
      educationlocation: String,
      educationdegree: String,
      educationmajor: String,
      educationyearofpassing: Number,
      educationcurrentcgpa: Number,
    },
  ],
  experience: [
    {
      experiencedetailsid: { type: String, default: uuid.v1 },
      companyname: String,
      experiencetitle: String,
      experiencelocation: String,
      experiencestartdate: String,
      experienceenddate: String,
      experienceworkdescription: String,


    },
  ],


});


student.plugin(uniqueValidator);
module.exports = mongoose.model('student', student);
