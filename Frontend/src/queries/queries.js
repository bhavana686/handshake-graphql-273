import { gql } from 'apollo-boost';

const getAllcompanyjobsQuery = gql`
    query($companyid: String){
    getAllcompanyjobs(companyid:$companyid) {
         jobid
         jobtitle
         jobpostingdate
         jobapplicationdeadline
         joblocation
         jobsalary
         jobdescription
         jobcategory
        }
    }
`;


const getStudentsAppliedtojobQuery = gql`
    query($jobid: String){
        getStudentsAppliedtojob(jobid:$jobid) {
            applicationid
            name
            mail
            studentid
            applicationstatus
        }
    }
`;
const getAllotherstudentsQuery = gql`
    query($studentid: String){
        getAllotherstudents(studentid: $studentid){
            studentid
            name
            mail
            collegename
           skillset
           
        }
    }
`;
const getAllstudentsQuery = gql`
    query{
        getAllstudents{
            studentid
            name
           mail
            collegename
           skillset
           
        }
    }
`;


const getCompanyprofileQuery = gql`
    query($companyid: String){
        getCompanyprofile(companyid:$companyid){
            companyname
            companymail
            companylocation
            companydescription
           
        }
    }
`;

const getStudentprofileQuery = gql`
    query($studentid: String){
        getStudentprofile(studentid:$studentid){
        name
        mail
        dateofbirth
        city
        state
        country
        phonenumber
        careerobjective
        skillset
        education{
            educationdetailsid
            collegename
            educationlocation
            educationdegree,
            educationmajor
            educationyearofpassing
            educationcurrentcgpa
            
        },
        experience{
            experiencedetailsid
            companyname
            experiencetitle
            experiencelocation
            experiencestartdate
            experienceenddate
            experienceworkdescription
            
        }
           
        }
    }
`;




const getstudentsAppliedjobsQuery = gql`
    query($studentid: String){
        getstudentsAppliedjobs(studentid:$studentid){

            companyid
            jobtitle
            jobpostingdate
            jobapplicationdeadline
            joblocation
            jobsalary
            jobdescription
            jobcategory
            jobid
            applications {
                applicationid
                applicationstatus

            }
           
            
           
           
        }
    }
`;

const getAlljobsQuery = gql`
    query($studentid: String){
        getAlljobs(studentid:$studentid){

           
            jobid
            companyid
            jobtitle
            jobpostingdate
            jobapplicationdeadline
            joblocation
            jobsalary
            jobdescription
            jobcategory
            applications{
                applicationid
                applicationstatus
                name
                mail
                studentid
            }

            companyname
           
           
        }
    }
`;







export { getAllcompanyjobsQuery, getstudentsAppliedjobsQuery,getStudentsAppliedtojobQuery ,getAllstudentsQuery,  getStudentprofileQuery,getAllotherstudentsQuery,getCompanyprofileQuery,getAlljobsQuery};