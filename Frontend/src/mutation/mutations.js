import { gql } from 'apollo-boost';

const addCompanyMutation = gql`
    mutation AddCompany($companyname: String, $companymail: String, $password: String, $companylocation: String){
        addCompany(companyname: $companyname, companymail: $companymail, password: $password, companylocation: $companylocation){
            message
            status
        }
       
    }
`;

const addStudentMutation = gql`
    mutation AddStudent($name: String, $mail: String, $password: String, $collegename: String){
        addStudent(name: $name, mail: $mail, password: $password, collegename: $collegename){
            message
            status
        }
    }
`;

const studentLoginMutation = gql`
    mutation studentLogin($mail: String, $password: String){
        studentLogin(mail: $mail, password: $password){
            message
            status
        }
    }
`;
const companyLoginMutation = gql`
    mutation studentLogin($companymail: String, $password: String){
        companyLogin(companymail: $companymail, password: $password){
            message
            status
        }
    }
`;

const addJobMutation = gql`
    mutation addJob($companyid: String, $jobtitle: String, $jobpostingdate: String, $jobapplicationdeadline: String,
        $joblocation:String , $jobsalary: String, $jobdescription: String, $jobcategory: String){
            addJob(companyid:$companyid,jobtitle: $jobtitle,jobpostingdate: $jobpostingdate, jobapplicationdeadline: $jobapplicationdeadline,
            joblocation: $joblocation , jobsalary:$jobsalary ,jobdescription: $jobdescription, jobcategory: $jobcategory){
            message
            status
        }
       
    }
`;

const updateCompanyMutation = gql`
    mutation updateCompanyMutation($companyid: String,$companyname: String, $companylocation: String, $companydescription: String){
        updateCompany(companyid: $companyid,companyname: $companyname, companylocation: $companylocation,  companydescription: $companydescription){
            message
            status
        }
       
    }
`;


const updateStudentbasicMutation = gql`

mutation($studentid: String, $name: String, $dateofbirth: String, $city: String,$state: String, $country: String){
    updateStudentbasic(studentid: $studentid, name: $name, dateofbirth: $dateofbirth, city: $city, state: $state, country: $country) {
        status
        message
    }
    
}
`;


const updateStudentcareerMutation = gql`
mutation($studentid: String, $careerobjective: String){
    updateStudentcareer(studentid: $studentid, careerobjective: $careerobjective) {
        status
        message
    }
    
}
`;




const updateStudentcontactMutation = gql`
mutation($studentid: String, $mail: String,$phonenumber: String){
    updateStudentcontact(studentid: $studentid, mail: $mail, phonenumber: $phonenumber) {
        status
        message
    }
    
}
`;

const applyJobMutation = gql`
    mutation applyJobMutation($jobid: String,$studentid: String, $name: String, $mail: String,$applicationstatus:String){
        applyJob(jobid: $jobid,studentid: $studentid, name: $name,  mail: $mail,applicationstatus:$applicationstatus){
            message
            status
        }
       
    }
`;


//            educationdetailsid:this.props.educationrecord.educationdetailsid,
//            collegename: this.state.collegename,
//            educationlocation: this.state.educationlocation,
//            educationdegree: this.state.educationdegree,
//            educationmajor: this.state.educationmajor,
//            educationyearofpassing: this.state.educationyearofpassing,
//            educationcurrentcgpa: this.state.educationcurrentcgpa,


const updateStudentEducationMutation = gql`
mutation($studentid: String, $educationdetailsid: String,$collegename: String, $educationlocation: String,$educationdegree: String, $educationmajor: String,$educationyearofpassing: String, $educationcurrentcgpa: String){
    updateStudentEducation(studentid: $studentid, educationdetailsid: $educationdetailsid,collegename: $collegename, educationlocation: $educationlocation,educationdegree: $educationdegree, educationmajor: $educationmajor,educationyearofpassing: $educationyearofpassing, educationcurrentcgpa: $educationcurrentcgpa) {
        status
        message
    }
    
}
`;
const addEducationMutation = gql`
mutation($studentid: String,$collegename: String, $educationlocation: String,$educationdegree: String, $educationmajor: String,$educationyearofpassing: String, $educationcurrentcgpa: String){
    addEducation(studentid: $studentid, collegename: $collegename, educationlocation: $educationlocation,educationdegree: $educationdegree, educationmajor: $educationmajor,educationyearofpassing: $educationyearofpassing, educationcurrentcgpa: $educationcurrentcgpa) {
        status
        message
    }
    
}
`;





const updateStudentExperienceMutation = gql`
mutation($studentid: String, $experiencedetailsid: String,$companyname: String, $experiencetitle: String,$experiencelocation: String, $experiencestartdate: String,$experienceenddate: String, $experienceworkdescription: String){
    updateStudentExperience(studentid: $studentid, experiencedetailsid: $experiencedetailsid,companyname: $companyname, experiencetitle: $experiencetitle,experiencelocation: $experiencelocation, experiencestartdate: $experiencestartdate,experienceenddate: $experienceenddate, experienceworkdescription: $experienceworkdescription) {
        status
        message
    }
    
}
`;


const addExperienceMutation = gql`
mutation($studentid: String,$companyname: String, $experiencetitle: String,$experiencelocation: String, $experiencestartdate: String,$experienceenddate: String, $experienceworkdescription: String){
    addExperience(studentid: $studentid,companyname: $companyname, experiencetitle: $experiencetitle,experiencelocation: $experiencelocation, experiencestartdate: $experiencestartdate,experienceenddate: $experienceenddate, experienceworkdescription: $experienceworkdescription) {
        status
        message
    }
    
}
`;

export {addCompanyMutation,updateStudentExperienceMutation ,addExperienceMutation,updateStudentEducationMutation, addEducationMutation, updateStudentbasicMutation,updateStudentcontactMutation,updateStudentcareerMutation, addStudentMutation, studentLoginMutation,companyLoginMutation,addJobMutation,updateCompanyMutation,applyJobMutation};