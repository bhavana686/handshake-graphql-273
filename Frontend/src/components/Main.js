import React, {Component} from 'react';
import {Route} from 'react-router-dom';
import {rooturl} from '../webConfig';

import Navbar from './Navbar';
import CompanySignup from './CompanySignup';
import StudentSignup from './StudentSignup';
import StudentLogin from './StudentLogin';
import CompanyLogin from './CompanyLogin';
import Companylandingpage from './company/Companylandingpage';
import Addjobpage from './company/Addjobpage';

import  Studentappliedtojob from './company/Studentappliedtojob';
import Studentlandingpage from './company/Studentlandingpage';
import Companydisplayprofile from './company/Companydisplayprofile';
import Jobsearch from './student/Jobsearch';
import Studentpage from './student/Studentpage';
import Applicationpage from './student/Applicationpage';
import Studentprofile from './student/Studentprofile/Studentprofile';
import Studentprofileout  from './student/Studentprofile/Studentprofileout';
import Companyprofileout  from './company/Companyprofileout';




class Main extends Component {
    render() {
        return (
            <div>
                 <Route path="/" component={Navbar}/>
                  <Route path="/StudentSignup" component={StudentSignup}/>
                <Route path="/StudentLogin" component={StudentLogin}/>
                <Route path="/CompanySignup" component={CompanySignup}/>
                <Route path="/CompanyLogin" component={CompanyLogin}/>
                <Route path="/Companylandingpage" component={Companylandingpage}/>
                <Route path="/Addjobpage" component={Addjobpage}/>
                <Route path="/Studentappliedtojob/:id" component={Studentappliedtojob}/>
                <Route path="/Studentlandingpage" component={Studentlandingpage}/>
                <Route path="/Companydisplayprofile" component={Companydisplayprofile}/>
                <Route path="/Jobsearch" component={Jobsearch}/>
                <Route path="/Studentpage" component={Studentpage}/>
                <Route path="/Applicationpage" component={Applicationpage}/>
                <Route path="/Studentprofile/:id" component={Studentprofile}/>
                <Route path="/Studentprofileout/:id" component={Studentprofileout}/>
                <Route path="/Companyprofileout/:id" component={Companyprofileout}/>
        
        
            </div>
        )
    }
}
export default Main;