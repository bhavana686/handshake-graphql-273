import React, { Component } from 'react';
import { Redirect } from 'react-router';
import { Link } from 'react-router-dom';
import { graphql } from 'react-apollo';
import { addStudentMutation } from '../mutation/mutations';





class StudentSignup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name : "",
            mail : "",
            password: "",
            collegename : "",
            Flag: false,
            successFlag: false
            
        };
        
    }
    onChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    onSubmit = async (e) => {
        //prevent page from refresh
        e.preventDefault();

        let mutationResponse = await this.props.addStudentMutation({
            variables: {
                name: this.state.name,
                mail: this.state.mail,
                password: this.state.password,
                collegename : this.state.collegename
            }
        });
        let response = mutationResponse.data.addStudent;
        if (response) {
            if (response.status === "200") {
                this.setState({
                    successFlag: true,
                    Flag: true
                });
            } else {
                this.setState({
                    message: response.message,
                    Flag: true
                });
            }
        }
    }

   render() {
        //redirect based on successful signup
        let redirectVar = null;
        let message = "";
        if (sessionStorage.getItem("token")) {
            redirectVar = <Redirect to="/Studentsignup" />
        }
        else if (this.state.successFlag && this.state.Flag)  {
            alert("You have registered successfully");
            redirectVar = <Redirect to="/StudentLogin" />
        }
        else if (this.state.message === "STUDENT_EXISTS" && this.state.Flag) {
            message = "Email id is already registered"
        }
        
        return (
            <div>

             {redirectVar}
           
    <div class="container">
                <div class="row">
                <div class="col-lg-10 col-xl-9 mx-auto">
                <div class="card card-signin flex-row my-5">
                
                <div class="card-body">
                <h5 class="card-title text-center"> STUDENT SIGN IN</h5>
                <form onSubmit={this.onSubmit}>
                                       
                                        <div class="form-group">
                                            <input type="email" class="form-control" name="mail" onChange={this.onChange} placeholder="Email Id" pattern="^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$'%&*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])$" title="Please enter valid email address" required />
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control" name="password" onChange={this.onChange} placeholder="Password" required />
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="name" onChange={this.onChange} placeholder="Student name" required />
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="collegename" onChange={this.onChange} placeholder="College Name" required />
                                        </div>
                                        <div style={{ color: "#ff0000" }}>{message}</div><br />
                                        <button type="submit" class="btn btn-primary">Signup</button><br /><br/>
                                        <div>Already have an account? <Link to='/StudentLogin'>Login</Link></div>
                                        
                                    </form>
                
                </div>
                </div> 
                </div>
                </div> 
                </div>  
                </div>
        )
       

    }
}



export default graphql(addStudentMutation, { name: "addStudentMutation" })(StudentSignup);