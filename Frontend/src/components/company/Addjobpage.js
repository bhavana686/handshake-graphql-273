import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { Link } from 'react-router-dom';
import 'react-dropdown/style.css'
import MenuItem from '@material-ui/core/MenuItem';
import { graphql } from 'react-apollo';
import {  compose, withApollo } from 'react-apollo';
import { getAllcompanyjobsQuery } from '../../queries/queries';
import Select from '@material-ui/core/Select';
import { addJobMutation } from '../../mutation/mutations';


const names = [
    'fulltime',
    'internship',
    'oncampus',
    'parttime',
  ];

  
 

class Addjobpage extends Component {

    constructor(props) {
        //Call the constrictor of Super class i.e The Component
        super(props);
       
        //maintain the state required for this component
        this.state = {
            jobtitle: "",
            jobpostingdate: "",
            jobapplicationdeadline: "",
            joblocation: "",
            jobsalary: "",
            jobdescription: "",
            jobcategory: "",
            jc:[],
            authFlag: 0
        }
        //Bind the handlers to this class
       
        this.handleChange=this.handleChange.bind(this);
        this.onChange = this.onChange.bind(this);
        this.submitJob=this.submitJob.bind(this);

    }
    
    onChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

  
   handleChange = event => {
    this.setState({
        jobcategory: event.target.value
    })
   
  };
  submitJob = async (e) => {
    e.preventDefault();
    var data = {
        companyid: sessionStorage.getItem('companyid'),
        jobtitle: this.state.jobtitle,
        jobpostingdate: this.state.jobpostingdate,
        jobapplicationdeadline: this.state.jobapplicationdeadline,
        joblocation: this.state.joblocation,
        jobsalary: this.state.jobsalary,
        jobdescription: this.state.jobdescription,
        jobcategory: this.state.jobcategory
    }
    let response = await this.props.addJobMutation({
        variables: data,
        refetchQueries: [{
            query: getAllcompanyjobsQuery ,
            variables: { companyid: sessionStorage.getItem("companyid"),
            fetchPolicy: 'no-cache' }
        }]
    })
    console.log(response.data)
    console.log(response.data.addJob.status)
    response = response.data.addJob;
    if (response.status == 200) {
        console.log("hi")
        this.setState({
            authFlag: 1
        })
      
    }
    
     else {
        this.setState({
            authFlag: 2
        })
    }
    console.log(this.state.authFlag)
}

    render() {
        let redirectval = null;
        let random = null;
        console.log(this.state.authFlag)
        if (sessionStorage.getItem('loggintype') !== 'company') {
            redirectval = <Redirect to='/CompanySignup' />
        }
        if (this.state.authFlag == 1)
            redirectval = <Redirect to ='/Companylandingpage' />
        else if (this.state.authFlag == 2)
            random = <p> could not add job </p>
        else{
            random = null;

        }

        return (
            <div>
               

                {redirectval}
                <br />
                
                <form onSubmit={this.submitPost}>
                    <div class="container">


                        <div class="row">
                            <div class="col-lg-10 col-xl-9 mx-auto">
                                <div class="card card-signin flex-row my-5">

                                    <div class="card-body">
                                        <h5 class="card-title text-center"> POST A JOB</h5>
                                    </div>

                                    <div class="form-label-group">
                                        <label class="control-label col-sm-2" for="jobtitle">Title:</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="jobtitle" id="jobtitle" onChange={this.onChange} class="form-control" placeholder="Job Title" required />
                                     
                                    </div><br/><br/>
                                    <div class="form-label-group">
                                        <label class="control-label col-sm-2" for="jobpostingdate">PostingDate:</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="jobpostingdate" id="jobpostingdate" onChange={this.onChange} class="form-control" placeholder="Posting Date" required />
                                        </div>
                                    </div><br/><br/>
                                    <div class="form-label-group">
                                    <label class="control-label col-sm-2" for="jobapplicationdeadline">Applicationdeadline:</label>
                                    <div class="col-sm-10">

                                    
                                            <input type="text" name="jobapplicationdeadline" id="jobapplicationdeadline" onChange={this.onChange} class="form-control" placeholder="Application Deadline" required />
                                        </div>
                                    </div><br/><br/>
                                    <div class="form-label-group">
                                        <label class="control-label col-sm-2" for="joblocationl">location:</label>
                                        <div class="col-sm-10">
                                        <input type="text" name="joblocation" id="joblocation" onChange={this.onChange} class="form-control" placeholder="location" required />
                                </div>
                                </div><br /><br />
                                <div class="form-label-group">
                                    <label class="control-label col-sm-2" for="jobsalary">salary</label>
                                    <div class="col-sm-10">
                                    
                                    <input type="salary" name="jobsalary" pattern="[0-9]+" id="jobsalary" onChange={this.onChange} class="form-control" placeholder="Salary" required />
                                </div>
                            </div><br /><br />
                            <div class="form-label-group">
                                <label class="control-label col-sm-2" for="jobdescription">jobdescription:</label>
                                <div class="col-sm-10">
                                <input type="text" name="jobdescription" id="jobdescription" onChange={this.onChange} class="form-control" placeholder="Job Description" required />
                            </div><br /><br />
                        </div>
                        <div class="form-label-group">
                        <label class="control-label col-sm-2" for="jobcategory">jobcategory</label>    
                            <div class="col-sm-10">
                            
                            
       
        <Select
        
          id="dobcategory"
          value={this.state.jobcategory}
          onChange={this.handleChange}
        >
          <MenuItem value={names[0]} >Fulltime</MenuItem>
          <MenuItem value={names[3]}>Parttime</MenuItem>
          <MenuItem value={names[2]}>OnCampus</MenuItem>
          <MenuItem value={names[1]}>Internship</MenuItem>

        </Select>
    
                        </div>
                    </div>
                    <br /><br />
                    <div class="form-group">
                    <button onClick={this.submitJob} class="btn btn-success" type="submit">Add</button>&nbsp;

                      
                         <Link to="/Companylandingpage"><button type="submit" class="btn btn-success">Cancel</button></Link>

                    </div>
                    {random}
        </div>
        </div>
        </div>
        </div>
        </div>
        </form>
        </div>
    
        )
    }
}





export default compose(
    graphql(addJobMutation, { name: "addJobMutation" })
)(Addjobpage);