        
import React, { Component } from 'react';
//import '../../App.css';


import CardContent from '@material-ui/core/CardContent';

import Avatar from '@material-ui/core/Avatar';
import { getCompanyprofileQuery} from '../../queries/queries';
import { updateCompanyMutation } from '../../mutation/mutations';
import {  compose, withApollo } from 'react-apollo';







export class Companyprofileout extends Component {
    constructor(props) {
        super(props);
        this.state={
            profile:"",
            companyid:"",
        }
        const { match: { params } } = this.props
        console.log(params.id)
        this.setState({
            companyid: params.id,

        })
        console.log(this.state.companyid)
        
        }
        
        
    
        componentDidMount(props) {
          

            this.getcompanyprofile();
          
        }
    
        getcompanyprofile = async() =>{
            const { match: { params } } = this.props
            const data1 = {
                companyid: params.id,
    
    
            }
            console.log(this.state.companyid)
           const { data } = await this.props.client.query({
               query:  getCompanyprofileQuery,
               variables: { companyid: data1.companyid},
               fetchPolicy: 'no-cache'
           })
           
           console.log(data.getCompanyprofile)
           
           this.setState({
                profile : data.getCompanyprofile,
             
           })
       }


    


    render() {
        let profiledetails = null;
        let basicdetails=null;
        let contactdetails=null;
     
        // if(this.props.profile!=null)
        // {
        //    profiledetails = (
        //     <CardContent style={{ textAlign: "-webkit-right", paddingTop: "10px" }} >
              
        //         <div style={{ textAlign: "-webkit-center" }}>
        //             {this.props.profile.profilepic === null ? (
        //                 <Avatar className="changePhoto" title="Upload profile pic"  variant="circle" >
                         
        //                 </Avatar>
        //             ) : (
        //                     <Avatar className="changePhoto" title="Change profile pic"  variant="circle" src={this.props.profile?this.props.profile.profilepic:""}  style={{ cursor: "pointer", width: "110px", height: "110px", margin: "15px", border: "0.5px solid" }} />
        //                 )}
        //         </div>
        //         <div style={{ textAlign: "-webkit-center" }}>
        //         <h2>{this.props.profile.companyname}</h2>

        //         </div>
               
                
        //     </CardContent>
        // )
       
        
        if(this.state.profile!=null){
         basicdetails=
             (
                <div>


                    <br></br>
                    <h4> Name:{this.state.profile.companyname?this.state.profile.companyname:""}</h4>
                    <h4>Location: {this.state.profile.companylocation?this.state.profile.companylocation:""}</h4>
                    <h4>Description:{this.state.profile.companydescription?this.state.profile.companydescription:""}</h4>



                </div>
            )



        
        
    
         contactdetails =(
        
                <div>


                    <br></br>
                    <h4> Mail:{this.state.profile.companymail?this.state.profile.companymail:""}</h4>
                    <h4>Phonenumber: {this.state.profile.companyphonenumber?this.state.profile.companyphonenumber:""}</h4>



                </div>
            )
         }



        
        
    
        return (
            <div>

                <div class="paddingleft15">


                    <div class="form-group row" paddingleft>
                        <div class="col-lg-1"></div>
                        <div class="col-lg-3">
                            <div class="well">
                                <h1>{profiledetails}</h1>
                            </div>


                        </div>

                        <div class="col-lg-7">
                            <div class="well">
                                <div class="row">
                                    <div class="col-lg-9">
                                        <h3>Basic Details</h3>
                                        {basicdetails}
                                    </div>
                                   
                                </div>
                             

                                </div>
                                <div class="well">
                                <div class="row">
                                    <div class="col-lg-9">
                                        <h3>Contact Details</h3>
                                    </div>
                                 
                                </div>
                                {contactdetails}
                            


                                </div>



                            </div>


                        </div>
                        </div>
                        </div>

              



        );

    }
}




export default  withApollo(Companyprofileout);