
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Redirect } from 'react-router';
import { graphql } from 'react-apollo';
import {  getStudentsAppliedtojobQuery } from '../../queries/queries';
import {  compose, withApollo } from 'react-apollo';





class Studentappliedtojob extends Component {
  
    constructor(props) {
        super(props);
        const { match: { params } } = this.props
        this.state = {
            jobid:params.id,
            studentdetails: [],
            applicationstatus: "",
            applicationid: "",
            msg: "",
            authFlag: 0,
            showresume: false,
            currentindex: 0,
            applications: [],

        }

       

    }

    //get the books data from backend  


 componentDidMount(){
 
        this.getstudentsapplied()
   
}

getstudentsapplied = async() =>{
       const { data } = await this.props.client.query({
           query: getStudentsAppliedtojobQuery ,
           variables: { jobid: this.state.jobid },
           fetchPolicy: 'no-cache'
       })
       console.log(this.state.jobid)
       this.setState({
           applications: data.getStudentsAppliedtojob
       })
   }


render() {
   
    let details = null;

    if (this.state.applications.length > 0) {
        details = this.state.applications.map((studentdetails, index) => {
            return (<div>
                <div class="well" text-right>
                    <h5>Student name:{studentdetails.name}</h5>
                    <h5>Student mail:   {studentdetails.mail}</h5>

                    <h3>change Application status</h3>

                    <select class="form-control" id="status" onChange={this.changestatus} onClick={() => this.setAppstatus(studentdetails.applicationid)}>
                        <option selected={studentdetails.applicationstatus === "Pending" ? true : false} >Pending</option>
                        <option selected={studentdetails.applicationstatus === "Reviewed" ? true : false}>Reviewed</option>
                        <option selected={studentdetails.applicationstatus === "Declined" ? true : false}>Declined</option>
                    </select><br></br>
                 

                            <Link to={"/Studentprofileout/" + studentdetails.studentid} className="btn btn-primary">View Profile</Link>
                </div>
            </div>

            )
        })


    }

    return (
        <div>

            <div class="paddingleft15">


                <div class="form-group row" paddingleft>
                    <div class="col-lg-2"></div>
                    <div class="col-lg-9">  <h2>List of students applied to job</h2>





                        {details}

                      



                    </div>
                </div>
            </div>
        </div>
    )
}

}

export default withApollo(Studentappliedtojob);
