import React,{ Component } from "react";
import { Link } from 'react-router-dom';
import { Redirect } from 'react-router';
import { graphql } from 'react-apollo';
import { getAllcompanyjobsQuery } from '../../queries/queries';
import {  compose, withApollo } from 'react-apollo';





class Companylandingpage extends Component
{
    constructor(props)
    {
      super();
        this.state = 
    {
        companyjoblist :[],   
    }
}
 componentDidMount(){
     this.getjobsofCompany()

}
getjobsofCompany = async() =>{
    const { data } = await this.props.client.query({
        query: getAllcompanyjobsQuery,
        variables: { companyid: sessionStorage.getItem("companyid") },
        fetchPolicy: 'no-cache'
    })
    
    this.setState({
        companyjoblist : data.getAllcompanyjobs
    })
}

render() {
  
    let displayform=null;
    

    if(this.state.companyjoblist?this.state.companyjoblist.length>0:"")
        {
        
        displayform =  (  


           this.state.companyjoblist.map((jobdetails,index) => {
          return(
            
            
                <div>
                    <div class="well" text-right>
                         <h5>job id:{jobdetails.jobid}</h5>
                        <h5>job Title:   {jobdetails.jobtitle}</h5>
                        <h5>job posting date:  {jobdetails.jobpostingdate}</h5>
                        <h5>job application deadline:  {jobdetails.jobapplicationdeadline}</h5>
                        <h5>job location :  {jobdetails.joblocation}</h5>
                        <h5>job salary:  {jobdetails.jobsalary}</h5>
                        <h5>job Description:  {jobdetails.jobdescription}</h5>
                        <h5>job Category:  {jobdetails.jobcategory}</h5>
                        <Link to={"/Studentappliedtojob/"+jobdetails.jobid}   className="btn btn-primary">Students Applied</Link>
                        
                    </div>
                    </div>
             
          )
      }))
        }
        else{
            displayform =  (  <div>NO JOBS POSTED </div>)

        }
      
      

      return (
             <div>
    
                <div class="paddingleft15">
                    <div class="form-group row" paddingleft>

                    <div class="col-lg-10"> </div>
                   
                    <div class="col-lg-1"><Link to="/Addjobpage" className="btn btn-primary">Add job</Link> </div>
                    </div>
                      
                    <div class="form-group row" paddingleft>
                        <div class="col-lg-2"></div>
                        <div class="col-lg-9">  <h2>List of jobs posted by company</h2>
                    
       
                        
                        
                    
                        {displayform}
                   

            
                        </div>
                        
                        </div>
                        </div>

            
            </div>
          
           
           
           
        )
    }

}



export default withApollo(Companylandingpage);
