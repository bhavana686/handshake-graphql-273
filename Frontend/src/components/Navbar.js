/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from 'react';
import { Link } from 'react-router-dom';


//create the Navbar Component
class Navbar extends Component {
    constructor(props) {
        super(props);
        this.handleLogout = this.handleLogout.bind(this);

    }
    //handle logout to destroy the cookie
    handleLogout = () => {

        window.sessionStorage.clear();


    }
    render() {

        let navLogin = null;

        if (sessionStorage.getItem('loggintype')) {

            let profile = null;
            let details = null;
            let eventstab = null;
            let studentstab = null;
            let jobs = null;
            let messages = null;
            let applications = null;
            if (sessionStorage.getItem('loggintype') === 'company') {
                profile = (<li><Link to={"/Companydisplayprofile/" + sessionStorage.getItem('companyid')} onClick={this.companyprofile} ><span></span>Profile</Link></li>)
                eventstab = (

                    <li><Link to="/Companyeventpage" onClick={this.companyevents}><span></span>Events</Link></li>
                )
                jobs = (

                    <li><Link to="/Companylandingpage" onClick={this.companyevents}>Jobs</Link></li>

                )
                studentstab = (

                    <li><Link to="/Studentlandingpage" onClick={this.companyevents}><span></span>Students</Link></li>
                )
                messages = (

                    <li><Link to="/Messages" onClick={this.companyevents}><span></span>Messages</Link></li>
                )


            }
            else {
                profile = (<li><Link to={"/Studentprofile/" + sessionStorage.getItem('studentid')} onClick={this.studentprofile}><span></span>Profile</Link></li>)
                eventstab = (

                    <li><Link to="/Studenteventpage" onClick={this.studentevents}>Events</Link></li>

                )
                jobs = (

                    <li><Link to="/Jobsearch" onClick={this.studentevents}>Jobs</Link></li>

                )
                applications = (

                    <li><Link to="/Applicationpage" onClick={this.studentapplications}>Application</Link></li>

                )
                studentstab = (

                    <li><Link to="/Studentpage" onClick={this.studentapplications}>Student</Link></li>
                )
                messages = (

                    <li><Link to="/Messages" onClick={this.studentevents}><span></span>Messages</Link></li>
                )



            }
            navLogin = (
                <div>
                    <ul class="nav navbar-nav navbar-right">
                        {jobs}
                        <ul class="nav navbar-nav navbar-left">
                            {studentstab}
                            <ul class="nav navbar-nav navbar-left">
                                {applications}
                                <ul class="nav navbar-nav navbar-left">
                                    {eventstab}
                                    <ul class="nav navbar-nav navbar-left">
                                        {details}
                                        <ul class="nav navbar-nav navbar-left">
                                            {profile}
                                            <ul class="nav navbar-nav navbar-left">
                                                {messages}

                                                <ul class="nav navbar-nav navbar-left">

                                                    <li><Link to="/" onClick={this.handleLogout}><span class="glyphicon glyphicon-log-out"></span>Logout</Link></li>
                                                    <ul class="nav navbar-nav navbar-right">   </ul>
                                                </ul>
                                            </ul>
                                        </ul>
                                    </ul>
                                </ul>
                            </ul>

                        </ul>
                    </ul>
                </div>
            );
        }
        else {
            navLogin = (
                <ul class="nav navbar-nav">
                    <li><Link to="/CompanySignup">Company</Link></li>
                    <li><Link to="/StudentSignup">Student</Link></li>
                </ul>
            );
        }
        let redirectVar = null;
        return (
            <div>
                {redirectVar}
                <nav class="navbar navbar-inverse">
                    <div class="container-fluid">
                        <div class="navbar-header text-left">
                            <a class="navbar-brand">Handshake</a>
                        </div>

                        {navLogin}

                    </div>
                </nav>

            </div>
        )
    }
}

export default Navbar;