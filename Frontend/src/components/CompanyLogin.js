import React, { Component } from 'react';
import { Redirect } from 'react-router';
import { Link } from 'react-router-dom';
import { graphql } from 'react-apollo';
import { companyLoginMutation } from '../mutation/mutations';
const jwt_decode = require('jwt-decode');

class CompanyLogin extends Component {
    //call the constructor method
    constructor(props) {
        //Call the constrictor of Super class i.e The Component
        super(props);
        //maintain the state required for this component
        this.state = {
            Flag: false,
            successFlag: false
        };
    }

    onChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    //submit Login handler to send a request to the node backend
    onSubmit = async (e) => {
        e.preventDefault();
        let mutationResponse = await this.props.companyLoginMutation({
            variables: {
                companymail: this.state.companymail,
                password: this.state.password,
            }
        });
        let response = mutationResponse.data.companyLogin;
        if (response) {
            if (response.status === "200") {
                this.setState({
                    successFlag: true,
                    data: response.message,
                    Flag: true
                });
            } else {
                this.setState({
                    message: response.message,
                    Flag: true
                });
            }
        }
    }

    render() {
        let redirectVar = null;
        let message = ""
        if (this.state.successFlag) {
            let token = this.state.data;
            sessionStorage.setItem("token", token);
            var decoded = jwt_decode(token.split(' ')[1]);
            console.log(decoded)
            sessionStorage.setItem("companyid", decoded.companyid);
            sessionStorage.setItem("companyname", decoded.companyname);
            sessionStorage.setItem("companymail", decoded.companymail);
            sessionStorage.setItem("loggintype", decoded.loggintype);
            redirectVar = <Redirect to="/Companylandingpage" />
        }            
        else if (this.state.message === "NO_COMPANY" && this.state.Flag) {
            message = "No company with this email id";
        }
        else if (this.state.message === "INCORRECT_PASSWORD" && this.state.Flag) {
            message = "Incorrect Password";
        }

        return (
            <div>

            {redirectVar}
          
   <div class="container">
               <div class="row">
               <div class="col-lg-10 col-xl-9 mx-auto">
               <div class="card card-signin flex-row my-5">
               
               <div class="card-body">
               <h5 class="card-title text-center"> COMPANY LOG IN</h5>
               <form onSubmit={this.onSubmit}>
                                      
                                        <div class="form-group">
                                           <input type="email" class="form-control" name="companymail" onChange={this.onChange} placeholder="Email Id" pattern="^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$'%&*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])$" title="Please enter valid email address" required />
                                       </div>
                                       <div class="form-group">
                                           <input type="password" class="form-control" name="password" onChange={this.onChange} placeholder="Password" required />
                                       </div>
                                      
                                       <div style={{ color: "#ff0000" }}>{message}</div><br />
                                       <button type="submit" class="btn btn-primary">Log in</button><br /><br/>
                                       <div>Already have an account? <Link to='/CompanySignup'>Signin</Link></div>
                                       
                                   </form>
               
               </div>
               </div> 
               </div>
               </div> 
               </div>  
               </div>
        )
    }
}

export default graphql(companyLoginMutation, { name: "companyLoginMutation" })(CompanyLogin);