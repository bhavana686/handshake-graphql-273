
import React, { Component } from 'react';



import IconButton from '@material-ui/core/IconButton';
import {Edit} from '@material-ui/icons';
import { getStudentprofileQuery } from '../../../queries/queries';
import { updateStudentcontactMutation } from '../../../mutation/mutations';
import {  compose , withApollo } from 'react-apollo';




export class Contactinfo extends Component {
    constructor() {
        super();
        this.state = {
       
            authFlag: 0,
          
            edit: false,

        }
           this.onChange = this.onChange.bind(this);
        this.handleedit = this.handleedit.bind(this);
            this.changeHandler = this.changeHandler.bind(this);
    
       
    }
    onChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }
    
    //get the books data from backend  
//     componentDidMount() {
//         const data ={
//             studentid:sessionStorage.getItem('studentid')

//         };
//         this.props.getStudentprofile(data);


       
// }
// componentWillReceiveProps(nextProps) {
//     if (nextProps.profile) {
//         var { profile } = nextProps;

//         var userData = {
//             add: false,
//             edit: false,
//             mail: profile.mail || this.state.mail,
//             phonenumber: profile.phonenumber || this.state.phonenumber,

//         };

//         this.setState(userData);
//     }
// }

componentWillReceiveProps(nextProps) {
    this.setState({
        studentdetails: nextProps.studentdetails,
        mail: nextProps.studentdetails.mail,
        phonenumber: nextProps.studentdetails.phonenumber,
       
    })
}

   
    // submitEdit = (e) => {
       
     
    //     e.preventDefault();

    //     const data = {
    //         studentid: sessionStorage.getItem('studentid'),
    //         mail:this.state.mail,
    //         phonenumber:this.state.phonenumber
    //     }
    //     this.handleedit();

    //     this.props.updateStudentcontactdetails(data);   
    // }


    submitEdit = async (e) => {
        e.preventDefault();
        let response = await this.props.client.mutate({
            mutation: updateStudentcontactMutation,
            variables: {
                studentid: sessionStorage.getItem("studentid"),
               mail:this.state.mail,
               phonenumber:this.state.phonenumber,
            }
            
        })
     
        console.log(response.data.updateStudentcontact)
        if (response.data.updateStudentcontact.status == 200) {
            this.handleedit();
            console.log(this.state.edit)
            this.props.getstudentProfile()
        } else {
           
            this.props.getstudentProfile()
        }
    }


        
    
    handleedit = () => {
        this.setState({
            edit: !this.state.edit

        })
    }
    changeHandler = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

   render()
   {

        
        let editform = null;

        let details;

       
        if (this.state.edit) {
           
            editform =

                (
                    <div>



                        <form onSubmit={this.submitPost}>
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-8 col-xl-7 mx-auto">
                                        <div class="card card-signin flex-row my-5">
                                            <div class="form-label-group">
                                                <label class="control-label col-sm-1" for="name"> Mail:</label>
                                                <div class="col-sm-3">
                                                    <input type="email" name="mail" id="mail" value={this.state.mail}  onChange={this.onChange} class="form-control" required />

                                                </div><br /><br />
                                                <div class="form-label-group">
                                                    <label class="control-label col-sm-1" for="">Phone Number:</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" name="phonenumber" id="phonenumber" value={this.state.phonenumber}  onChange={this.onChange} class="form-control" required />
                                                    </div>
                                                </div><br /><br />
                                                
                                                <div class="form-group">
                                                    <button onClick={this.submitEdit} class="btn btn-success" type="submit">Edit</button>&nbsp;
                                                    <button onClick={this.handleedit} class="btn btn-success" type="submit">Cancel</button>&nbsp;
        </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>


                );
        }
        else{
            // if (this.props.profile.length>0) {
            //     details =
            //         (
            //              <div>
            //                 <br></br>
            //                 <h4> Mail:{this.props.profile[0].mail}</h4>
                           
            //                 <h4>Phonenumber:{this.props.profile[0].phonenumber}</h4>
                           

            //             </div>
            //         )
            // }
            // else {
            //     console.log("2");
                details =
                    (
                        <div>


                            <br></br>
                            <h4> Mail:{this.state.mail}</h4>
                           
                           <h4>Phonenumber:{this.state.phonenumber}</h4>
                           
                        </div>
                    )

            }
    

        return (
            <div>
            <div class="row">
               <div class="col-lg-9">
               <h3>Contact Information</h3>
               </div> 
               <div class="col-lg-1">
               <IconButton style={{fontSize:30}} onClick={()=>this.handleedit()}><Edit/></IconButton>
               </div> 
               </div>
          
               
               
              

                   {details}
                   {editform}
                   
                   

            



           </div>





        );
    }
}

 export default withApollo(Contactinfo);

