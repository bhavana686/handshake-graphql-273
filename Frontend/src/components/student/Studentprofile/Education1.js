import React, { Component } from 'react';
//import '../../App.css';

import Education2 from './Education2';

import { getStudentprofileQuery } from '../../../queries/queries';
import { updateStudentEducationMutation,addEducationMutation} from '../../../mutation/mutations';
import {  compose , withApollo } from 'react-apollo';







export class Education1 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            profile:{},
            studentdetails:{},
            education:[],
           
            authFlag: 0,
            add: false,
            collegename:"",
            educationlocation:"",
            educationdegree:"", 
            educationmajor:"",
            educationyearofpassing:"",
            educationcurrentcgpa:""

        }
        this.handleadd = this.handleadd.bind(this); 
        this.onChange = this.onChange.bind(this);
        this.getstudentProfile=this.getstudentProfile.bind(this);

    }
    onChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }
    componentWillReceiveProps(nextProps) {
        this.setState({
            studentdetails: nextProps.studentdetails,
            education: nextProps.studentdetails.education
        })
    }
 
  
    getstudentProfile = () => {
        this.props.getstudentProfile()
    }

       


    submitEducation = async (e) => {
        e.preventDefault();

        const data = {
            studentid: sessionStorage.getItem('studentid'),
            collegename: this.state.collegename,
            educationlocation: this.state.educationlocation,
            educationdegree: this.state.educationdegree,
            educationmajor: this.state.educationmajor,
            educationyearofpassing: this.state.educationyearofpassing,
            educationcurrentcgpa: this.state.educationcurrentcgpa,

        }
        let response = await this.props.client.mutate({
            mutation: addEducationMutation,
            variables: data
        })
        response = response.data.addEducation;
        if (response.status == 200) {
            this.setState({
                add: false,
            })
            this.props.getstudentProfile()
        } 
        else {
            this.setState({
                addSchool: true
            })
            this.props.getstudentProfile()
        }
    }
       
      

    

    handleadd = () => {
        this.setState({
            add: !this.state.add

        })
    }

    changeHandler = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }


 
    render() {
        let details = null;

        let addform = (<div><button onClick={this.handleadd}>
            Add Education
          </button>

        </div>)
          console.log(this.props.profile)
        if (this.state.add) {

                addform =

                    (
                        <div>



                            <form onSubmit={this.submitPost}>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-lg-10 col-xl-9 mx-auto">
                                            <div class="card card-signin flex-row my-5">
                                                <div class="form-label-group">
                                                    <label class="control-label col-sm-2" for="collgename">College Name:</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" name="collegename" id="collegename" onChange={this.onChange} class="form-control" required />

                                                    </div><br /><br />
                                                    <div class="form-label-group">
                                                        <label class="control-label col-sm-2" for="educationlocation">Location:</label>
                                                        <div class="col-sm-3">
                                                            <input type="text" name="educationlocation" id="educationlocation" onChange={this.onChange} class="form-control" required />
                                                        </div>
                                                    </div><br /><br />
                                                    <div class="form-label-group">
                                                        <label class="control-label col-sm-2" for="educationdegree">Degree:</label>
                                                        <div class="col-sm-3">


                                                            <input type="text" name="educationdegree" id="educationdegree" onChange={this.onChange} class="form-control" required />
                                                        </div>
                                                    </div><br /><br />
                                                    <div class="form-label-group">
                                                        <label class="control-label col-sm-2" for="educationmajor">Major:</label>
                                                        <div class="col-sm-3">
                                                            <input type="text" name="educationmajor" id="educationmajor" onChange={this.onChange} class="form-control" required />
                                                        </div>
                                                    </div><br /><br />
                                                    <div class="form-label-group">
                                                        <label class="control-label col-sm-2" for="educationyearofpassing">Year of passing</label>
                                                        <div class="col-sm-3">
                                                            <input type="year" name="educationyearofpassing" id="educationyearofpassing" onChange={this.onChange} class="form-control" required />
                                                        </div>
                                                    </div><br /><br />
                                                    <div class="form-label-group">
                                                        <label class="control-label col-sm-2" for="educationcurrentcgpa">Current CGPA:</label>
                                                        <div class="col-sm-3">
                                                            <input type="text" name="educationcurrentcgpa" id="educationcurrentcgpa" onChange={this.onChange} class="form-control" required />
                                                        </div><br /><br />
                                                    </div>
                                                    <br /><br />
                                                    <div class="form-group">
                                                        <button onClick={this.submitEducation} class="btn btn-success" type="submit">Add</button>&nbsp;
                                            <button onClick={this.handleadd} class="btn btn-success" type="submit">Cancel</button>&nbsp;
        </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>


                    );
            }
            
            else{
            
                if (this.state.education!=null) {
                   
                  
                         details= this.state.education.map((education,index) => {
                            return (
                                   <Education2 educationrecord={education} getstudentProfile={this.getstudentProfile}></Education2>
                                  
                                
                          )
                     
                            
                        
                            })
                         
            }
               
              
            }



            return (
                <div>
                    <h3>Education</h3>

                    {details}

                    {addform}






                </div>





            );
        }
    }


export default withApollo(Education1);
    