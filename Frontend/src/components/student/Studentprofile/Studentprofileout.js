import React, { Component } from 'react';
import dateFormat from 'dateformat';
import Chip from '@material-ui/core/Chip';
import Avatar from '@material-ui/core/Avatar';
import CardContent from '@material-ui/core/CardContent';
import { Link } from 'react-router-dom';
import { getStudentprofileQuery } from '../../../queries/queries';

import {  compose , withApollo } from 'react-apollo';




export class Studentprofileout extends Component {

    constructor(props) {
        super(props);
        const { match: { params } } = this.props
        
        this.state =
        {
            studentlist:{},
            studentid: params.id,
            education: {},
            experience: {},

        }

    }

    componentDidMount() {
        this.getstudentProfile();
    }

    getstudentProfile = async() => {
        const { data } = await this.props.client.query({
            query: getStudentprofileQuery,
            variables: { studentid: this.state.studentid },
            fetchPolicy: 'no-cache'
          })
          this.setState({
            studentlist: data.getStudentprofile,
            experience: data.getStudentprofile?data.getStudentprofile.experience:"",
            education: data.getStudentprofile?data.getStudentprofile.education:"",
            
          })

    }


//     displayskills() {
//         if (this.props.profile != null){
//         if (this.props.profile.skillset!=null) {
//             let skilllist = this.props.profile.skillset.split(",");

//             let result = [];

//             for (let i = 0; i < skilllist.length; i++) {
//                 result.push(<Chip label={skilllist[i]} onDelete={() => this.handleDelete(skilllist[i])} style={{ "font-size": "15px", "margin": "8px", }}></Chip>)

//             }
//             return result;
//         }
//         else
//             return ("");

//     }
// }



    render() {
        let basicdetails = null;
        let contactdetails = null;
        let educationdetails = null;
        let experiencedetails = null;
        let careerdetails=null;
        


        if (this.state.studentlist != null)
        {
            basicdetails =
                (

                    <div>


                        <br></br>
                        <h4> Name:{this.state.studentlist.name}</h4>
                        <h4>Date of birth: { this.state.studentlist.dateofbirth}</h4>
                        <h4>City:{ this.state.studentlist.city}</h4>
                        <h4>State:{this.state.studentlist.state}</h4>
                        <h4>Country:{this.state.studentlist.country}</h4>


                    </div>
                )



            contactdetails =
                (
                    <div>


                        <br></br>
                        <h4>Mail:{this.state.studentlist.mail}</h4>
                        <h4>Phonenumber:{this.state.studentlist.phonenumber}</h4>


                    </div>
                )
     if(this.state.education.length>0){


            educationdetails = (this.state.education?this.state.education.map((studenteducationinfo, index) => {
                return (
                    <div>
                        <div class="row">
                            <div class="col-lg-1"><span class="glyphicon glyphicon-education" style={{ fontSize: 30 }}></span></div>
                            <div class="col-lg-10">
                                <h4>College Name:{studenteducationinfo.collegename}</h4>
                                <h4>Location:{studenteducationinfo.educationlocation}</h4>
                                <h4>Degree:{studenteducationinfo.educationdegree}</h4>
                                <h4>Major:{studenteducationinfo.educationmajor}</h4>
                                <h4>Year of passing:{studenteducationinfo.educationyearofpassing}</h4>
                                <h4>Current CGPA:{studenteducationinfo.educationcurrentcgpa}</h4>
                            </div>
                            <div class="col-lg-1">

                            </div>
                        </div>
                    </div>
                )
            }):"" )
        }
        
        if(this.state.experience.length>0){
            experiencedetails = (this.state.experience?this.state.experience.map((studentexperienceinfo, index) => {
                return (
                    <div>
                        <div class="row">
                            <div class="col-lg-1"><span class="glyphicon glyphicon-briefcase" style={{ fontSize: 30 }}></span></div>
                            <div class="col-lg-10">


                                <h4>Company Name:{studentexperienceinfo.companyname}</h4>
                                <h4>Title:{studentexperienceinfo.experiencetitle}</h4>
                                <h4>Location:{studentexperienceinfo.experiencelocation}</h4>
                                <h4>Startdate: {studentexperienceinfo.experiencestartdate}</h4>
                                <h4>Endadte: {studentexperienceinfo.experienceenddate}</h4>
                                <h4>Work Description:{studentexperienceinfo.experienceworkdescription}</h4>


                            </div>
                            <div class="col-lg-1">

                            </div>
                        </div>
                    </div>


                )
            }):"")
        }
        


            careerdetails = 
                (
                    <div>
                        <div class="row">
                            <div class="col-lg-1"></div>
                            <div class="col-lg-11">



                                <h4>{this.state.studentlist.careerobjective}</h4>

                            </div>

                        </div>
                    </div>
                )



        


           
            //     profiledetails = (
            //         <CardContent style={{ textAlign: "-webkit-right", paddingTop: "10px" }} >
                      
            //             <div style={{ textAlign: "-webkit-center" }}>
            //                 {this.props.profile.profilepic === null ? (
            //                     <Avatar className="changePhoto" title="Upload profile pic"  variant="circle" >
                                  
            //                     </Avatar>
            //                 ) : (
            //                         <Avatar className="changePhoto" title="Change profile pic"  variant="circle" src={this.props.profile?this.props.profile.profilepic:""}  style={{ cursor: "pointer", width: "110px", height: "110px", margin: "15px", border: "0.5px solid" }} />
            //                     )}
            //             </div>
            //             <div style={{ textAlign: "-webkit-center" }}>
            //             <h4>{this.props.profile.name}</h4>
            //             <h4>{this.props.profile.mail}</h4>
            //             <Link to={{ pathname: "/Messages",
            //                            state: {
            //                                  studentlist:this.props.profile
            //                                 } 
            //             }}   className="btn btn-primary">Message</Link>
                               
                      
        
            //             </div>
                       
                        
            //         </CardContent>
               
            // )
                            }

                    



            return (
                <div>

                    <div class="paddingleft15">


                        <div class="form-group row" paddingleft>
                            <div class="col-lg-1"></div>
                            <div class="col-lg-3">
                                

                                <div class="well">
                                    <h3>Basic Details</h3>

                                    {basicdetails}



                                </div>
                                
                                <div class="well">
                                    <h3>Contact Information </h3>


                                    {contactdetails}


                                </div>

                            </div>

                            <div class="col-lg-7">
                                <div class="well">
                                    <h3>Career Objective</h3>


                                    {careerdetails}


                                </div>
                                <div class="well">
                                    <h3>Education Details</h3>



                                    {educationdetails}

                                </div>

                                <div class="well">
                                    <h3>Experience Details</h3>


                                    {experiencedetails}

                                </div>

                            </div>





                        </div>

                    </div>
                </div>


            )



            }
        }
        
    
    


    

export default withApollo(Studentprofileout);