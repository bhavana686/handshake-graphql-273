import React, { Component } from 'react';
//import '../../App.css';

import IconButton from '@material-ui/core/IconButton';
import { Edit } from '@material-ui/icons';

import dateFormat from 'dateformat';

import { getStudentprofileQuery } from '../../../queries/queries';
import { updateStudentExperienceMutation,addExperienceMutation} from '../../../mutation/mutations';
import {  compose , withApollo } from 'react-apollo';






export class Experience2 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            experiencerecord: [],
            authFlag: 0,

            edit: false,
            companyname: "",
            experiencetitle: "",
            experiencelocation: "",
            experiencestartdate: "",
            experienceenddate: "",
            experienceworkdescription: "",

           
        }

        this.onChange = this.onChange.bind(this);
        this.handleedit = this.handleedit.bind(this);
        this.changeHandler = this.changeHandler.bind(this);
        this.subDelete=this.subDelete.bind(this)
        


    }


    componentDidMount() {

        this.setState({
            companyname: this.props.experiencerecord.companyname,
            experiencetitle: this.props.experiencerecord.experiencetitle,
            experiencelocation: this.props.experiencerecord.experiencelocation,
            experiencestartdate:this.props.experiencerecord.experiencestartdate,
            experienceenddate:this.props.experiencerecord.experienceenddate,
            experienceworkdescription: this.props.experiencerecord.experienceworkdescription,

        });

    }
    componentWillReceiveProps(nextProps) {
        this.setState({     
                companyname: nextProps.experiencerecord.companyname,
                experiencetitle: nextProps.experiencerecord.experiencetitle,
                experiencelocation: nextProps.experiencerecord.experiencelocation, 
                experiencestartdate:nextProps.experiencerecord.experiencestartdate ,
                experienceenddate:nextProps.experiencerecord.experienceenddate ,
                experienceworkdescription: nextProps.experiencerecord.experienceworkdescription 
        })
    

    }
     

  onChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }
    submitEdit = async (e) => {
        e.preventDefault();
        const data = {
             studentid: sessionStorage.getItem('studentid'),
            experiencedetailsid:this.props.experiencerecord.experiencedetailsid,
            companyname: this.state.companyname,
            experiencetitle:this.state.experiencetitle,
            experiencelocation: this.state.experiencelocation,
            experiencestartdate: this.state.experiencestartdate,
            experienceenddate: this.state.experienceenddate,
            experienceworkdescription: this.state.experienceworkdescription
    
        }
        let response = await this.props.client.mutate({
            mutation: updateStudentExperienceMutation,
            variables: data
               
            
        })
     
        console.log(response.data.updateStudentExperience)
        if (response.data.updateStudentExperience.status == 200) {
            this.setState({
             
                edit:false
            })
            
            this.props.getstudentProfile()
        } else {
           
            this.props.getstudentProfile()
        }
    }
    

    
    subDelete = () => {
        console.log(this.props.experiencerecord)
        console.log(this.state.companyname)

       
        const data = {
            studentid: sessionStorage.getItem('studentid'),
            experiencedetailsid:this.props.experiencerecord.experiencedetailsid,
           
        }
      this.props.deleteExperience(data);
      console.log(this.props.experiencerecord)
        console.log(this.state.companyname)
      
        this.setState({
       
          //companyname: this.props.profile.experience[0].companyname,
         // experiencetitle:this.props.experiencerecord.experiencetitle,
        //  experiencelocation: this.props.experiencerecord.experiencelocation,
        //  experiencestartdate: this.props.experiencerecord.experiencestartdate,
        //  experienceenddate: this.props.experiencerecord.experienceenddate,
        //  experienceworkdescription: this.props.experiencerecord.experienceworkdescription,

           
            edit:false
        })
        console.log(this.props.experiencerecord)
        console.log(this.state.companyname)
        

    }
    
    handleedit = () => {
        this.setState({
            edit: !this.state.edit

        })
    }
    changeHandler = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }




   
    render() {
       
        let  editform=null;

        let details;
        
    
                            
        if(this.state.edit)
        {
            details=null;
            editform = 
            
            (  
                <div>
  
  
  
                <form onSubmit={this.submitPost}>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-10 col-xl-9 mx-auto">
                                <div class="card card-signin flex-row my-5">
                                    <div class="form-label-group">
                                        <label class="control-label col-sm-2" for="companyname">Company Name:</label>
                                        <div class="col-sm-3">
  
                                            <input type="text" name="companyname" id="companyname" value={this.state.companyname}  onChange={this.onChange} class="form-control" required />
  
                                        </div><br /><br />
                                        <div class="form-label-group">
                                                    <label class="control-label col-sm-2" for="experiencetitle">Title:</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" name="experiencetitle" id="experiencetitle" value={this.state.experiencetitle}  onChange={this.onChange} class="form-control" required />
                                                    </div>
                                                </div><br /><br />
                                                <div class="form-label-group">
                                                    <label class="control-label col-sm-2" for="experiencelocation">Location:</label>
                                                    <div class="col-sm-3">


                                                        <input type="text" name="experiencelocation" id="experiencelocation" value={this.state.experiencelocation} onChange={this.onChange} class="form-control" required />
                                                    </div>
                                                </div><br /><br />
                                                <div class="form-label-group">
                                                    <label class="control-label col-sm-2" for="experiencestartdate">Start Date:</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" name="experiencestartdate" id="experiencestartdate" value={this.state.experiencestartdate} onChange={this.onChange} class="form-control" required />
                                                    </div>
                                                </div><br /><br />
                                                <div class="form-label-group">
                                                    <label class="control-label col-sm-2" for="experienceenddate">End Date</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" name="experienceenddate" id="experienceenddate" value={this.state.experienceenddate}  onChange={this.onChange} class="form-control" required />
                                                    </div>
                                                </div><br /><br />
                                                <div class="form-label-group">
                                                    <label class="control-label col-sm-2" for="experienceworkdescription">Work Description:</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" name="experienceworkdescription" value={this.state.experienceworkdescription} id="experienceworkdescription" onChange={this.onChange} class="form-control" required />
                                                    </div><br /><br />
                                                </div>
                                                <br /><br />
                                                <div class="form-group">
                                        
                                         
                                            <button onClick={this.submitEdit} class="btn btn-success" type="submit">Submit</button>&nbsp;
                                            <button onClick={this.handleedit} class="btn btn-success" type="submit">Cancel</button>&nbsp;
                                            <button onClick={this.subDelete} class="btn btn-danger" type="submit">Delete</button>&nbsp;
        </div>
  
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
  
             
            );  
        }
        else{
            details= (
                
                    <div> 
                    <div class="row">
                    <div class="col-lg-1"><span class="glyphicon glyphicon-briefcase"  style={{fontSize:30}}></span></div>
                    <div class="col-lg-10">
                       
                           
                                   
                    <h4>Company Name:{this.state.companyname}</h4>
                    <h4>Title:{this.state.experiencetitle}</h4>
                    <h4>Location:{this.state.experiencelocation}</h4>
                    <h4>Startdate: {this.state.experiencestartdate}</h4>
                    <h4>Endadte: {this.state.experienceenddate}</h4>
                    <h4>Work Description:{this.state.experienceworkdescription}</h4>
                            </div>
                            <div class="col-lg-1"> 
                            <IconButton style={{fontSize:30}} onClick={()=>this.handleedit()}><Edit/></IconButton>
                              </div>
                            </div>
                       </div>
                
                
            
            
            
            )
        }
       
              return (
        <div>
                
                
               {details}
               {editform}
              
               
             
              
              
              
                
               
               </div>
               
                
             
               
               
            );
        }
}



export default withApollo(Experience2);
