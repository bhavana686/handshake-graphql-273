import React, { Component } from 'react';
//import '../../App.css';

import Experience2 from './Experience2';

import { getStudentprofileQuery } from '../../../queries/queries';
import { updateStudentExperienceMutation,addExperienceMutation} from '../../../mutation/mutations';
import {  compose , withApollo } from 'react-apollo';







export class Experience1 extends Component {
    constructor() {
        super();
        this.state = {
            profile:{},
            experiencerecord:[],
            studentdetails:{},
            experience:[],
            authFlag: 0,
            add: false,
            companyname: "",
            experiencetitle: "",
            experiencelocation: "",
            experiencestartdate: "",
            experienceenddate: "",
            experienceworkdescription: ""

        }
        this.handleadd = this.handleadd.bind(this); 
        this.onChange = this.onChange.bind(this);  
        this.getstudentProfile=this.getstudentProfile.bind(this);


    }
   
    onChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            studentdetails: nextProps.studentdetails,
            experience: nextProps.studentdetails.experience
        })
    }
    getstudentProfile = () => {
        this.props.getstudentProfile()
    }

       
 
  
    submitExperience = async (e) => {
     
        e.preventDefault();

        const data = {
            studentid: sessionStorage.getItem('studentid'),
            
            companyname: this.state.companyname,
            experiencetitle:this.state.experiencetitle,
            experiencelocation: this.state.experiencelocation,
            experiencestartdate: this.state.experiencestartdate,
            experienceenddate: this.state.experienceenddate,
            experienceworkdescription: this.state.experienceworkdescription,
      
        }
        let response = await this.props.client.mutate({
            mutation: addExperienceMutation,
            variables: data
        })
        response = response.data.addExperience;
        console.log(response)
        if (response.status == 200) {
            this.setState({
                add: false,
            })
            this.props.getstudentProfile()
        } 
        else {
            this.setState({
                addSchool: true
            })
            this.props.getstudentProfile()
        }
        console.log(this.state.companyname)
        console.log(this.state.studentdetails)
    }
       

    handleadd = () => {
        this.setState({
            add: !this.state.add

        })
    }

    changeHandler = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }


 
    render() {
        let details = null;

        let addform = (<div><button onClick={this.handleadd}>
            Add Experience
          </button>

        </div>)
          
        if (this.state.add) {

                addform =

                    (
                        <div>
                            <form onSubmit={this.submitPost}>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-lg-10 col-xl-9 mx-auto">
                                            <div class="card card-signin flex-row my-5">
                                            <div class="form-label-group">
                                        <label class="control-label col-sm-2" for="companyname">Company Name:</label>
                                        <div class="col-sm-3">
                                            <input type="text" name="companyname" id="companyname" onChange={this.onChange} class="form-control" required />

                                        </div><br /><br />
                                        <div class="form-label-group">
                                            <label class="control-label col-sm-2" for="experiencetitle">Title:</label>
                                            <div class="col-sm-3">
                                                <input type="text" name="experiencetitle" id="experiencetitle" onChange={this.onChange} class="form-control" required />
                                            </div>
                                        </div><br /><br />
                                        <div class="form-label-group">
                                            <label class="control-label col-sm-2" for="experiencelocation">Location:</label>
                                            <div class="col-sm-3">


                                                <input type="text" name="experiencelocation" id="experiencelocation" onChange={this.onChange} class="form-control" required />
                                            </div>
                                        </div><br /><br />
                                        <div class="form-label-group">
                                            <label class="control-label col-sm-2" for="experiencestartdate">Start Date:</label>
                                            <div class="col-sm-3">
                                                <input type="text" name="experiencestartdate" id="experiencestartdate" onChange={this.onChange} class="form-control" required />
                                            </div>
                                        </div><br /><br />
                                        <div class="form-label-group">
                                            <label class="control-label col-sm-2" for="experienceenddate">End Date</label>
                                            <div class="col-sm-3">
                                                <input type="text" name="experienceenddate" id="experienceenddate" onChange={this.onChange} class="form-control" required />
                                            </div>
                                        </div><br /><br />
                                        <div class="form-label-group">
                                            <label class="control-label col-sm-2" for="experienceworkdescription">Work Description:</label>
                                            <div class="col-sm-3">
                                                <input type="text" name="experienceworkdescription" id="experienceworkdescription" onChange={this.onChange} class="form-control" required />
                                            </div><br /><br />
                                        </div>
                                        <br /><br />
                                        <div class="form-group">
                                            <button onClick={this.submitExperience} class="btn btn-success" type="submit">Add</button>&nbsp;
                                            <button onClick={this.handleadd} class="btn btn-success" type="submit">Cancel</button>&nbsp;
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>


                    );
            }
            
            else{
                console.log(this.state.experience)
                if (this.state.experience!=null) {
                   
                         details= this.state.experience.map((experience,index) => {
                            return (
                                   <Experience2 experiencerecord={experience} getstudentProfile={this.getstudentProfile}></Experience2>
                                  
                                
                          )
                     
                            
                        
                            })
                    
                }

              
        
            }



            return (
                <div>
                    <h3>Experience </h3>

                    {details}

                    {addform}






                </div>





            );
        }
    }

    export default withApollo(Experience1);
    