import React, { Component } from 'react';
import dateFormat from 'dateformat';
import IconButton from '@material-ui/core/IconButton';
import { Edit } from '@material-ui/icons';
import { getStudentprofileQuery } from '../../../queries/queries';
import { updateStudentbasicMutation } from '../../../mutation/mutations';
import {  compose , withApollo } from 'react-apollo';
import TablePagination from '@material-ui/core/TablePagination';








export class Basicdetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            authFlag: 0,

            edit: false,
            name: "",
            dateofbirth: "",
            city: "",
            state: "",
            country: "",
        }
    
        this.onChange = this.onChange.bind(this);
        this.handleedit = this.handleedit.bind(this);
        this.changeHandler = this.changeHandler.bind(this);

    }
    componentWillReceiveProps(nextProps) {
        this.setState({
            studentdetails: nextProps.studentdetails,
            mail: nextProps.studentdetails.mail,
            name: nextProps.studentdetails.name,
            dateofbirth: nextProps.studentdetails.dateofbirth,
            city: nextProps.studentdetails.city,
            state: nextProps.studentdetails.state,
            country: nextProps.studentdetails.country,
        })
    }

   
    onChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }


    // submitEdit = (e) => {

       
    //     e.preventDefault();
    //     console.log(this.state.name)
       
    //     const data = {
    //         studentid: sessionStorage.getItem('studentid'),
    //         name: this.state.name,
    //         dateofbirth: this.state.dateofbirth,
    //         city: this.state.city,
    //         state: this.state.state,
    //         country: this.state.country
    //     }

    //     this.props.updateStudentbasicdetails(data);
    //     console.log(this.props.profile)
    //     this.handleedit();

    // }
    submitEdit = async (e) => {
        e.preventDefault();
        let response = await this.props.client.mutate({
            mutation: updateStudentbasicMutation,
            variables: {
                studentid: sessionStorage.getItem("studentid"),
                mail: this.state.mail,
                name: this.state.name,
                dateofbirth: this.state.dateofbirth,
                city: this.state.city,
                state: this.state.state,
                country: this.state.country,
            }
            
        })
        console.log(this.state.country)
        console.log(this.state.name)
        console.log(response.data.updateStudentbasic)
        if (response.data.updateStudentbasic.status == 200) {
            this.handleedit();
            console.log(this.state.edit)
            this.props.getstudentProfile()
        } else {
           
            this.props.getstudentProfile()
        }
    }

    handleedit = () => {
        this.setState({
            edit: !this.state.edit,
        })
    }
    changeHandler = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }




    render() {
        let editform = null;

        let details;

        if (this.state.edit) {
            details = null;

            editform =

                (
                    <div>



                        <form onSubmit={this.submitPost}>
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-10 col-xl-9 mx-auto">
                                        <div class="card card-signin flex-row my-5">
                                            <div class="form-label-group">
                                                <label class="control-label col-sm-2" for="name"> Name:</label>
                                                <div class="col-sm-2">
                                                    <input type="text" name="name" id="name" value={this.state.name} onChange={this.onChange} class="form-control" required />

                                                </div><br /><br />
                                                <div class="form-label-group">
                                                    <label class="control-label col-sm-2" for="dateofbirth">Date of birth:</label>
                                                    <div class="col-sm-2">
                                                        <input type="text" name="dateofbirth" id="dateofbirth" value={this.state.dateofbirth} onChange={this.onChange} class="form-control" required />
                                                    </div>
                                                </div><br /><br />
                                                <div class="form-label-group">
                                                    <label class="control-label col-sm-2" for="experiencelocation">City:</label>
                                                    <div class="col-sm-2">


                                                        <input type="text" name="city" id="city" value={this.state.city} onChange={this.onChange} class="form-control" required />
                                                    </div>
                                                </div><br /><br />
                                                <div class="form-label-group">
                                                    <label class="control-label col-sm-2" for="state">State:</label>
                                                    <div class="col-sm-2">
                                                        <input type="text" name="state" id="state" value={this.state.state} onChange={this.onChange} class="form-control" required />
                                                    </div>
                                                </div><br /><br />
                                                <div class="form-label-group">
                                                    <label class="control-label col-sm-2" for="country">Country</label>
                                                    <div class="col-sm-2">
                                                        <input type="text" name="country" id="country" value={this.state.country} onChange={this.onChange} class="form-control" required />
                                                    </div>
                                                </div><br /><br />

                                                <div class="form-group">
                                                    <button onClick={this.submitEdit} class="btn btn-success" type="submit">Edit</button>&nbsp;
                                                    <button onClick={this.handleedit} class="btn btn-success" type="submit">Cancel</button>&nbsp;
        </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>


                );
        }
        else {
            console.log(this.props.profile);
            console.log("1");
            // if (this.props.profile.length>0) {
            //     details =
            //         (
            //              <div>
            //                 <br></br>
            //                 <h4> Name:{this.props.profile[0].name}</h4>
            //                 <h4>Date of birth: {dateFormat(this.props.profile[0].dateofbirth, "mmmm dS, yyyy")}</h4>
            //                 <h4>City:{this.props.profile[0].city}</h4>
            //                 <h4>State:{this.props.profile[0].state}</h4>
            //                 <h4>Country:{this.props.profile[0].country}</h4>


            //             </div>
            //         )
            // }
            // else {
                console.log("2");
                details =
                    (
                        <div>


                            <br></br>
                            <h4> Name:{this.state.name}</h4>
                            <h4>Date of birth: {this.state.dateofbirth}</h4>
                            <h4>City:{this.state.city}</h4>
                            <h4>State:{this.state.state}</h4>
                            <h4>Country:{this.state.country}</h4>


                        </div>
                    )

           // }

        }


        return (
            <div>
                <div class="row">
                    <div class="col-lg-9">
                        <h3>Basic Details</h3>
                    </div>
                    <div class="col-lg-1">
                        <IconButton style={{ fontSize: 30 }} onClick={() => this.handleedit()}><Edit /></IconButton>
                    </div>
                </div>





                {details}
                {editform}







            </div>





        );
    }
}


export default withApollo(Basicdetails);

