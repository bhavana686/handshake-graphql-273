import React, { Component } from 'react';
//import '../../App.css';

import Education1 from './Education1';
import Experience1 from './Experience1';
import Skillset from './Skillset';
import Careerobjective from './Careerobjective';
import Contactinfo from './Contactinfo';
import Basicdetails from './Basicdetails';
import Profilepic from './Profilepic';
import { getStudentprofileQuery } from '../../../queries/queries';

import {  compose , withApollo } from 'react-apollo';





export class Studentprofile extends Component {

    constructor(props) {
        super(props);
        const { match: { params } } = this.props
       
        console.log(params.id)
        const data = {
            studentid: params.id,
            

        }
        sessionStorage.setItem('studentid' ,data.studentid);
        this.state = {
            studentdetails: {},
            education: {},
            experience: {},
          
        }
       
        
    } 
    componentDidMount() {
        this.getstudentProfile();
    }

    getstudentProfile = async() => {
        const { data } = await this.props.client.query({
            query: getStudentprofileQuery,
            variables: { studentid: sessionStorage.getItem("studentid") },
            fetchPolicy: 'no-cache'
          })
          this.setState({
            studentdetails: data.getStudentprofile,
            experience: data.getStudentprofile.experience,
            education: data.getStudentprofile.education,
            skills: data.getStudentprofile.skills,
            objective: data.getStudentprofile.career_objective
          })

    }
    render() {
        console.log(this.state.studentdetails)
       
            return (
                <div>

                    <div class="paddingleft15">


                        <div class="form-group row" paddingleft>
                            <div class="col-lg-1"></div>
                            <div class="col-lg-3">
                                 {/* <div class="well">
                                  <Profilepic></Profilepic>
                                    
                                </div> */}
                                <div class="well">
                                   
                                    <Basicdetails studentdetails={this.state.studentdetails} getstudentProfile={this.getstudentProfile} ></Basicdetails>
                              
 
                                </div>
                                {/* <div class="well">
                                   
                                   
                                    <Skillset studentdetails={this.state.studentdetails} getstudentProfile={this.getstudentProfile} ></Skillset>
                                 
                                   
                                </div> */}
                                <div class="well">
                                   
                                   
                                   <Contactinfo  studentdetails={this.state.studentdetails} getstudentProfile={this.getstudentProfile} ></Contactinfo>
                                
                                  
                               </div>

                            </div>

                            <div class="col-lg-7">
                                <div class="well">
                                   
                                   
                                    <Careerobjective  studentdetails={this.state.studentdetails} getstudentProfile={this.getstudentProfile} ></Careerobjective>
                                     
 
                                </div>
                                 <div class="well">
                                  
                                <Education1 studentdetails={this.state.studentdetails} getstudentProfile={this.getstudentProfile} ></Education1>
                                  
                                   
                                </div>

                                 <div class="well">
                                   
                                <Experience1 studentdetails={this.state.studentdetails} getstudentProfile={this.getstudentProfile} />
                                  


                                </div> 
                                   
                            </div>





                        </div>

                    </div>
                    </div>




                    );
             
                }
            }
    export default withApollo(Studentprofile);
            
       
