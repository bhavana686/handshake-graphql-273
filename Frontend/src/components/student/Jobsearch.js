import React from 'react';
import { Link } from 'react-router-dom';
import dateFormat from 'dateformat';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';

import TablePagination from '@material-ui/core/TablePagination';
import { graphql } from 'react-apollo';
import { getAlljobsQuery} from '../../queries/queries';
import { applyJobMutation } from '../../mutation/mutations';
import {  compose, withApollo } from 'react-apollo';





const names = [
    'joblocation',
    'jobpostingdate',
    'jobapplicationdeadline',
    'none'
];



class Jobsearch extends React.Component {


    constructor(props) {
        super(props);

        this.state = {
            page: 0,
            rowsPerPage: 5,
            file: null,
            companynameSearch: "",
            jobtitleSearch: "",
            applicationstatus: "",
            displaytext: "apply",
            text: "apply",
            sorttype: "",
            filter: {
                onfulltime: false,
                onparttime: false,
                oninternship: false,
                oncampus: false,
            },
            filterlist: [],
            templist: [],
            joblist: [],
            currentjob: [],
            apply: false,
           
            color1: "false",
            color2: "false",
            color3: "false",
            color4: "false",
            order: 1,



        }
        this.companynameSearchHandler = this.companynameSearchHandler.bind(this);
        this.jobtitleSearchHandler = this.jobtitleSearchHandler.bind(this);
        this.applyfilter = this.applyfilter.bind(this);
        this.applyforjob = this.applyforjob.bind(this);
        this.oncampusFilter = this.oncampusFilter.bind(this)
        this.onfulltimeFilter = this.onfulltimeFilter.bind(this)
        this.onparttimeFilter = this.onparttimeFilter.bind(this)
        this.oninternshipFilter = this.oninternshipFilter.bind(this)
        this.displayjob = this.displayjob.bind(this)
        this.onChangefile = this.onChangefile.bind(this);
        this.handleapply = this.handleapply.bind(this);

        this.handleChange = this.handleChange.bind(this);
        this.handlesort = this.handlesort.bind(this);
        this.handleorder = this.handleorder.bind(this);
       
    }


    componentDidMount() {

        this.getalljobs();

    }
    getalljobs= async() =>{
        const { data } = await this.props.client.query({
            query: getAlljobsQuery,
            variables: { studentid: sessionStorage.getItem("studentid") },
            fetchPolicy: 'no-cache'
        })
        console.log(data.getAlljobs)
        this.setState({
            joblist: data.getAlljobs,
            filterlist: data.getAlljobs,
            templist: data.getAlljobs,

        })
        
    }






    companynameSearchHandler(e) {
        this.applyfilter(e.target.value.toLowerCase(), this.state.jobtitleSearch.toLowerCase(), this.state.filter)
        this.setState({
            companynameSearch: e.target.value
        });


    }

    jobtitleSearchHandler(e) {
        this.applyfilter(this.state.companynameSearch.toLowerCase(), e.target.value.toLowerCase(), this.state.filter)

        this.setState({
            jobtitleSearch: e.target.value
        });

    }
    onfulltimeFilter() {
        let filter = {
            ...this.state.filter,
            onfulltime: !this.state.filter.onfulltime

        }
        this.applyfilter(this.state.companynameSearch.toLowerCase(), this.state.jobtitleSearch.toLowerCase(), filter)
        this.setState({
            filter: filter,
            color1: !this.state.color1
        });

    }
    onparttimeFilter() {

        let filter = {
            ...this.state.filter,
            onparttime: !this.state.filter.onparttime

        }
        this.applyfilter(this.state.companynameSearch.toLowerCase(), this.state.jobtitleSearch.toLowerCase(), filter)
        this.setState({
            filter: filter,
            color2: !this.state.color2
        });

    }
    oncampusFilter() {

        let filter = {
            ...this.state.filter,
            oncampus: !this.state.filter.oncampus

        }
        this.applyfilter(this.state.companynameSearch.toLowerCase(), this.state.jobtitleSearch.toLowerCase(), filter)
        this.setState({
            filter: filter,
            color4: !this.state.color4
        });


    }
    oninternshipFilter() {

        let filter = {
            ...this.state.filter,
            oninternship: !this.state.filter.oninternship

        }
        this.applyfilter(this.state.companynameSearch.toLowerCase(), this.state.jobtitleSearch.toLowerCase(), filter)
        this.setState({
            filter: filter,
            color3: !this.state.color3
        });

    }

    applyfilter = (p1, p2, filter) => {
        var resultlist = [];
        for (var i = 0; i < this.state.joblist.length; i++) {

            if ((this.state.joblist[i].jobtitle.toLowerCase().includes(p2) && this.state.joblist[i].companyname.toLowerCase().includes(p1))
                && (

                    (!filter.onfulltime && !filter.onparttime && !filter.oninternship && !filter.oncampus) ||
                    ((filter.onfulltime && this.state.joblist[i].jobcategory.includes("fulltime")) ||
                        (filter.onparttime && this.state.joblist[i].jobcategory.includes("parttime")) ||
                        (filter.oninternship && this.state.joblist[i].jobcategory.includes("internship")) ||
                        (filter.oncampus && this.state.joblist[i].jobcategory.includes("oncampus"))))

            ) {
                resultlist.push(this.state.joblist[i])
            }



        }
        this.setState({
            filterlist: resultlist,
            templist: resultlist,


        })

    }
    displayjob(index) {
        console.log(index);
        this.setState({
            displaytext: "apply",
            currentjob: this.state.filterlist[index]

        })
    }
    handleapply = () => {
        this.setState({
            apply: !this.state.apply,
            displaytext:"applied"

        })
    }
    handleChange = event => {
        // console.log(this.state.filterlist.jobapplicationdeadline.type())
        this.setState({
            sorttype: event.target.value
        })


    };
    handleorder = (n) => {
        // console.log(this.state.filterlist.jobapplicationdeadline.type())
        this.setState({
            order: n
        })
        this.handlesort();


    };
    applyforjob= async(e)=>
    {
        e.preventDefault();
        var data = {
            jobid:this.state.currentjob.jobid,
            studentid:sessionStorage.getItem('studentid'),
            name:sessionStorage.getItem('studentid'),
            mail:sessionStorage.getItem('mail'),
            applicationstatus:"pending"
        }
        let response =  await this.props.client.mutate({
            mutation: applyJobMutation,
            variables: data,
          
           
        })
        console.log(response.data)
        console.log(response.data.applyJob.status)
        //response = response.data.upadateCompany;
        if (response.data.applyJob.status == 200) {
            this.getalljobs();
           // this.getcompanyprofile();
            
            this.setState({
                text:"applied",
                authFlag: 1,
                editbasic: false

            })
          
        }
        
         else {
            this.setState({
                authFlag: 2
            })
        }
        console.log(this.state.authFlag)
    }

       
      
      

      
      
    

    onChangefile(e) {
        this.setState({
            file: e.target.files[0]
        });
    }
    applyjob = (e) => {

        e.preventDefault();
        const formData = new FormData();
        formData.append('jobid', this.state.currentjob.jobid)
        formData.append('studentid', sessionStorage.getItem('studentid'))
        formData.append('name', sessionStorage.getItem('name'))
        formData.append('mail', sessionStorage.getItem('mail'))

        formData.append('resume', this.state.file);
        formData.append('applicationstatus', 'pending');
        const config = {
            headers: {
                'content-type': 'multipart/form-data'
            }
        };
        this.props.applyForJob(formData, config);
      
                this.setState({
                    displaytext: "applied",
                    apply: false

                })
              
       

    }
    handlesort = () => {
        var list = []
        let n = this.state.order;
        console.log(this.state.sorttype)
        if (this.state.sorttype === "joblocation" || this.state.sorttype === "") {
            console.log("11")
            console.log(n)
            list = this.state.filterlist.sort(function (a, b) {
                return (n * ((a.joblocation > b.joblocation) - (b.joblocation > a.joblocation)));
            });
        }
        else if (this.state.sorttype === "none") {
            console.log("hi")
            console.log(n)
            console.log(this.state.templist)

            list = this.state.filterlist.sort(function (a, b) {
                return (n * ((a.jobtitle > b.jobtitle) - (b.jobtitle > a.jobtitle)));
            });
        }
        else if (this.state.sorttype === "jobapplicationdeadline") {
            console.log("22")
            console.log(n)
            list = this.state.filterlist.sort((a, b) => (n * ((new Date(a.jobapplicationdeadline) > new Date(b.jobapplicationdeadline))) - (new Date(b.jobapplicationdeadline) > new Date(a.jobapplicationdeadline))));
        }
        else
        {
            console.log("33")
            list = this.state.filterlist.sort((a, b)  => (n * ((new Date(a.jobpostingdate) > new Date(b.jobpostingdate))) - (new Date(b.jobpostingdate) > new Date(a.jobpostingdate))));


        }
        this.setState({
                filterlist: list === [] ? this.state.filterlist : list


            })
    }
    handleChangePage = (event, newPage) => {
        let currentjob = this.state.filterlist[newPage * this.state.rowsPerPage]
        this.setState({
            page: newPage,
            currentjob: currentjob,
            
        })
    };
    handleChangeRowsPerPage = (event) => {
        this.setState({
            rowsPerPage: (parseInt(event.target.value, 10)),
            page: 0,
        })


    };
    componentWillReceiveProps(nextProps) {
        if (nextProps.joblist) {
            var { joblist } = nextProps;

            this.setState({
                filterlist: joblist

            });
        }
    }
    render() {
        let sortform = null;
        let applyform = null;
        sortform = (<div>    <div class="paddingRight-15">
            <span class="glyphicon glyphicon-glass"></span>

            <Select

                id="sort"
                value={this.state.sorttype}
                onClick={this.handlesort}
                onChange={this.handleChange}

            >

                <MenuItem value={names[0]}   >location </MenuItem>
                <MenuItem value={names[1]} >postingdate</MenuItem>
                <MenuItem value={names[2]} >applicationdeadline</MenuItem>
                <MenuItem value={names[3]}  >none</MenuItem>


            </Select>
        </div>&nbsp;&nbsp;&nbsp;<div>

                <button style={{ backgroundColor: "#f0f8ff" }} onClick={() => this.handleorder(1)} >   <span class="glyphicon glyphicon-arrow-up"></span></button> &nbsp;
      <button style={{ backgroundColor: "#f0f8ff" }} onClick={() => this.handleorder(-1)}><span class="glyphicon glyphicon-arrow-down"></span></button></div>





        </div>)

        


        let currentjobdisplay = null;
        let status=this.state.currentjob.applications ?
                        this.state.currentjob.applications[0] ? this.state.currentjob.applications[0].applicationstatus : "" : ""

       console.log(status)
        if(!status)
         {
            
                applyform =(
                    <div> <button onClick={this.applyforjob} class="btn btn-primary" type="submit">{this.state.text}</button>&nbsp; </div>
                )


                
            }
            //         (<div>



            //             <form onSubmit={this.submitPost}>
            //                 <div class="container">

            //                     <div class="form-group">
            //                         <input type="file" class="applyform"
            //                             name="resume" id="resume" onChange={this.onChangefile} />
            //                     </div>


            //                 </div><br /><br />

            //                 <div class="form-group">
            //                     <button onClick={this.applyjob} class="btn btn-success" type="submit">submit</button>&nbsp;
            //                         <button onClick={this.handleapply} class="btn btn-success" type="submit">Cancel</button>&nbsp;



            //         </div>
            //             </form>
            //         </div>


            //         );
            // }
           
        else{
            applyform =

                    (<div><button >Applied
                      
                    </button>  </div>)


            }

        
        



        //iterate over books to create a table row
        let details =
            (
                (this.state.rowsPerPage > 0
                    ? this.state.filterlist.slice(this.state.page * this.state.rowsPerPage, this.state.page * this.state.rowsPerPage + this.state.rowsPerPage)
                    : this.state.filterlist
                ).map((filterlist, index) => {


                    return (


                        <div class="well" text-right>
                            <h5>Job Title:{filterlist.jobtitle}</h5>
                            <h5>Company Name: <Link to={"/Companyprofileout/" + this.state.currentjob.companyid}>{filterlist.companyname}</Link></h5>
                            <h5>Category: {filterlist.jobcategory}</h5>
                            <h5>location: {filterlist.joblocation}</h5>
                            <h5>Application deadline: {filterlist.jobapplicationdeadline}</h5>


                            <h5>Postingdate: {filterlist.jobpostingdate}</h5>

                            <button onClick={() => this.displayjob(index)} class="btn btn-primary" type="button">View details</button>



                        </div>


                    )
                }))



        if (this.state.currentjob?this.state.currentjob.jobtitle:"") {
            console.log(this.state.currentjob)
            currentjobdisplay = (
                <div>

                <h1> {this.state.currentjob.jobtitle}</h1>
                 
               
                <div class="form-group  row">
                    <div class="col-md-3">
                     <i class='far fa-clock'></i>&nbsp;{this.state.currentjob.jobapplicationdeadline}
                    </div>
                    <div class="col-md-3">
                    <i class="fa fa-map-marker"></i>&nbsp;{this.state.currentjob.joblocation}



                    </div>
                    <div class="col-md-3">
                    <i class="fa fa-dollar"></i>&nbsp;{this.state.currentjob.jobsalary}

                    </div>
                    <div class="col-md-3">
                    <span class="glyphicon glyphicon-briefcase"  style={{fontSize:15,color:"white"}}></span>&nbsp;{this.state.currentjob.jobcategory}<br></br><br></br>

                    </div>
                    </div>
                   

                 
           
                   
                  
                   
                    <span style={{ color: "#0489B1" }}><b>location:</b></span> {this.state.currentjob.joblocation}<br></br>
                    <span style={{ color: "#0489B1" }}><b>description:</b></span> {this.state.currentjob.jobdescription}<br></br>
                    <span style={{ color: "#0489B1" }}><b>Status:</b></span> <button type="button">{this.state.currentjob.applications ?
                        this.state.currentjob.applications[0] ? this.state.currentjob.applications[0].applicationstatus : "" : ""}
                        
                        </button><br></br><br></br>



                    <Link to={"/Companyprofileout/" + this.state.currentjob.companyid} className="btn btn-primary">Company Profile</Link>&nbsp;&nbsp;<br></br>
                    <br></br>

                    {applyform}


                </div>


            )

        }
        else
        {
            currentjobdisplay = (
                <div class="well" text-right>

                <h1> select job to view details</h1>
                </div>
            )


        }


        return (

            <div>
                <div class="form-group  row">
                    <div class="col-md-1"></div>
                    <div class="col-md-10">




                        <div class="well well-sm" >



                            <div class="form-group row">
                                <div class="col-md-1"></div>
                                <div class="col-md-4 paddingRight">
                                    <input type="text" onChange={this.jobtitleSearchHandler} class="form-control" name="search" placeholder=" Search by Job Titles" required />
                                </div>
                                <div class="col-md-1"></div>
                                <div class="col-md-4 paddingLeft">
                                    <input type="text" onChange={this.companynameSearchHandler} class="form-control" name="search" placeholder=" Search by Company name" required />

                                </div>
                            </div>
                            <div class="form-group  row">
                                <br></br>
                                <div class="col-md-1"></div>
                                <div class="col-md-2">
                                    <button style={{ backgroundColor: this.state.color1 ? "#f0f8ff" : "#3399ff" }} onClick={this.onfulltimeFilter}> Full Time</button>
                                </div>
                                <div class="col-md-2">
                                    <button style={{ backgroundColor: this.state.color2 ? "#f0f8ff" : "#3399ff" }} onClick={this.onparttimeFilter}> Part Time</button>
                                </div>
                                <div class="col-md-2">
                                    <button style={{ backgroundColor: this.state.color3 ? "#f0f8ff" : "#3399ff" }} onClick={this.oninternshipFilter}> Internship </button>
                                </div>
                                <div class="col-md-2">
                                    <button style={{ backgroundColor: this.state.color4 ? "#f0f8ff" : "#3399ff" }} onClick={this.oncampusFilter}> On Campus</button>
                                </div>

                            </div>


                        </div>
                        <div class="form-group  row">

                            <div class="col-md-6">


                                <div class="well well-sm">

                                    {sortform}
                                    <br></br>
                                    {details}
                                    <h1><TablePagination
                                        style={{ fontSize: 10 }}
                                        rowsPerPageOptions={[5, 10, 25]}
                                        component="div"
                                        count={this.state.filterlist.length}
                                        rowsPerPage={this.state.rowsPerPage}
                                        page={this.state.page}
                                        onChangePage={this.handleChangePage}
                                        onChangeRowsPerPage={this.handleChangeRowsPerPage} /></h1>

                                </div>
                            </div>
                            <div class="col-md-6">




                                {currentjobdisplay}




                            </div>

                            <div class="col-md-1"></div>
                        </div>
                    </div>





                </div>
            </div>
        )
    }
}




export default withApollo(Jobsearch);
