import React, { Component } from 'react';

import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { graphql } from 'react-apollo';
import { getAllotherstudentsQuery } from '../../queries/queries';
import {  compose, withApollo } from 'react-apollo';
import TablePagination from '@material-ui/core/TablePagination';




class Studentpage extends Component {
    constructor() {
        super();
        this.state = {
           
            studentSearch: "",
            filterlist: [],
            studentlist: [],
            page:0,
            rowsPerPage:5,
        }
        this.studentSearchHandler = this.studentSearchHandler.bind(this);
        this.applyfilter=this.applyfilter.bind(this);
    }
    componentDidMount(){
        this.getjobsofCompany()
   
   }
   getjobsofCompany = async() =>{
       const { data } = await this.props.client.query({
           query: getAllotherstudentsQuery,
           variables: { studentid: sessionStorage.getItem("studentid") },
           fetchPolicy: 'no-cache'
       })
       console.log(data)
       
       this.setState({
           studentlist : data.getAllotherstudents,
           filterlist : data.getAllotherstudents
       })
   }
  

    studentSearchHandler(e) {
        this.applyfilter(e.target.value.toLowerCase())

        this.setState({
            studentSearch: e.target.value
        });
        
    }
   
    applyfilter = (p1) =>
     {
        var resultlist = [];
        for (var i = 0; i < this.state.studentlist.length; i++) {

            if (this.state.studentlist[i].name.toLowerCase().includes(p1) || this.state.studentlist[i].collegename.toLowerCase().includes(p1))
            {
                resultlist.push(this.state.studentlist[i])
            }
            if(this.state.studentlist[i].skillset!=null && this.state.studentlist[i].skillset!=="" && this.state.studentlist[i].skillset.toLowerCase().includes(p1))
            {
                resultlist.push(this.state.studentlist[i])

            }

        }
        this.setState({
            filterlist:resultlist

        })

    }
    handleChangePage = (event, newPage) => {
        this.setState({
            page: newPage
        })
       };
       handleChangeRowsPerPage = (event) => {
        this.setState({ 
            rowsPerPage: (parseInt(event.target.value, 10)),
            page: 0,
         })
        
    
      };
    

   
        

    render() {
        
        
       let details=null;
        console.log(this.props.studentlist);
        console.log(this.state.filterlist);
        
          if(this.state.filterlist.length>0 && this.state.filterlist!=="")
          {
        //iterate over books to create a table row
           details =(
            (this.state.rowsPerPage > 0
                ?  this.state.filterlist.slice(this.state.page * this.state.rowsPerPage, this.state.page * this.state.rowsPerPage + this.state.rowsPerPage)
                : this.state.filterlist
              ).map((studentlist,index)=> {
           
            return (
                <div class="well" text-right>
               <div class="form-group row">
                    <div class="col-md-8">
            
                    <h5> Name:  {studentlist.name}</h5>
                    <h5> Mail :  {studentlist.mail}</h5>
                    <h5> Collegename :  {studentlist.collegename}</h5>
                  
                    <Link to={"/Studentprofileout/"+studentlist.studentid} 
                      className="btn btn-primary">View Profile</Link>      
                    </div>
                    <div class="col-md-3"><br></br><br></br><br></br> 
                    <Link to={{ pathname: "/Messages",
                                       state: {
                                             studentlist:studentlist
                                            } 
                        }}   className="btn btn-primary">Message</Link>
                               
                     
                    </div>
                </div>
                </div>


            )
        }))
    }
        return (
            <div class="paddingleft15">


                <div class="form-group row">
                    <div class="col-md-3"></div>
                    <div class="col-md-7">
                     
                        <div class="well" text-right>
                            
                           
                            <input type="text"  onChange={this.studentSearchHandler} class="form-control" name="studentsearch"
                                placeholder=" Search by student name,collegename" required /><br></br>
                                <div>{details}</div>
                                <h1><TablePagination 
                            style={{ fontSize: 10 }}
                          rowsPerPageOptions={[5, 10, 25]}
                          component="div"
                          count={this.state.filterlist.length}
                          rowsPerPage={this.state.rowsPerPage}
                          page={this.state.page}
                          onChangePage={this.handleChangePage}
                          onChangeRowsPerPage={this.handleChangeRowsPerPage}/></h1>
                    
                           
                            



                        </div>
                    </div>
                    <div class="col-md-2">
                
                    </div>

                </div>



            </div>

        )

    }
}

export default withApollo(Studentpage);
